export const constraints = {
    emailAddress: {
      presence: {
        allowEmpty: false,
        message: "^Ingresa tu correo"
      },
      email: {
        message: "^Ingresa un correo valido"
      }
    },
  };
  
  export default constraints;