import React, { Component } from 'react';
import { 
    Container, 
    Header, 
    Content, 
    Footer, 
    FooterTab, 
    Button, 
    Right, 
    Left,
    Body, 
    Icon, 
    Text, 
    Item, 
    Label, 
    Input,
    Picker,
    Title,
    Form,
    DatePicker 
} from 'native-base';
import {
  Alert
} from "react-native";
import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase("tesis4.db");
export default class AnatomyExample extends Component {
  constructor(){
    super()
    this.state = { 
        name1: '',
        name2: '', 
        surname1: '', 
        surname2: '', 
        country: 1, //Colombia
        city: 5, //Bogota
        phone: '', 
        age: '', 
        genre: 1,//Hombre
        email: '', 
        user: '', 
        password: '',
        countries:[],
        genres:[],
        errors: ''
    };
}
  async UNSAFE_componentWillMount() {
    var queryEstudiantes = 'CREATE TABLE IF NOT EXISTS STUDENTS ( \
      IDStudent       INTEGER      NOT NULL\
      , StudentName     VARCHAR (45) NOT NULL\
      , StudentName2    VARCHAR (45)\
      , StudentSurname  VARCHAR (45) NOT NULL\
      , StudentSurname2 VARCHAR (45)\
      , IDCountry INT\
      , IDCity INT\
      , StudentPhone    VARCHAR (13) NOT NULL\
      , StudentAge         INT          \
      , IDGenre         INT          NOT NULL\
      , StudentEmail    VARCHAR (60)\
      , StudentUser    VARCHAR (60)\
      , StudentPassword    VARCHAR (100)\
      , active          BINARY       DEFAULT 1\
      , PRIMARY KEY ( IDStudent AUTOINCREMENT )\
      , FOREIGN KEY ( IDCity ) REFERENCES City (IDCity)\
      , FOREIGN KEY ( IDGenre ) REFERENCES Genre (IDGenre) );';
      var params = [];
    db.transaction((tx) => {
      tx.executeSql(queryEstudiantes, params, (tx, results) => {
          console.log(results);
          // Alert.alert("Excelente", "Se ha creado la tabla de estudiantes ");
      }, function (tx, err) {
          Alert.alert("Espera un momento", "Error al crear la estructura de estudiantes en la BD: "+err);
          return;
      });
    });
    var queryNotas = 'CREATE TABLE IF NOT EXISTS EvaluationNotes ( \
      IDNote    INTEGER    NOT NULL\
      , IDStudent INTEGER    NOT NULL\
      , IDTheme   INTEGER    NOT NULL\
      , Note      DOUBLE     DEFAULT 0\
      , PRIMARY KEY ( IDNote AUTOINCREMENT )\
      , FOREIGN KEY ( IDStudent ) REFERENCES STUDENTS (IDStudent)\
      , FOREIGN KEY ( IDTheme ) REFERENCES Themes (IDTheme));';
      var params = [];
    db.transaction((tx) => {
      tx.executeSql(queryNotas, params, (tx, results) => {
          console.log(results);
          // Alert.alert("Excelente", "Se ha creado la tabla de notas ");
      }, function (tx, err) {
          Alert.alert("Espera un momento", "Error al crear la estructura de notas en la BD: "+err);
          return;
      });
    });
  }

  handleSave() {
    const { name1 } = this.state;
    const { name2 } = this.state;
    const { surname1 } = this.state;
    const { surname2 } = this.state;
    const { country } = this.state;
    const { city } = this.state;
    const { phone } = this.state;
    const { age } = this.state;
    const { genre } = this.state;
    const { email } = this.state;
    const { user } = this.state;
    const { password } = this.state;

    if (name1 != "" && surname1 != "" && country != "" && city != "" && phone != "" && age != "" && genre != "" && password!="" && user!="") {
        this.insert(name1, name2,surname1,surname2,country,city,phone,age, genre,email,user,password);
        this.navigatetologin();
    }
    else {
        Alert.alert("Un momento", "Verifique que los campos obligatorios esten llenos");
    }
  }

  navigatetologin=()=>{
    this.props.navigation.navigate('Login')//Tiene que estar dentro del componente para que el componente la encuentre
  } 
  onValueChangeGenre(value) {
    this.setState({
      genre: value
    });
  }
  onValueChangeCountry(value) {
    this.setState({
      country: value
    });
  }
setAge(fecha){
  var fecha_hoy = new Date();
  var fecha_nac = new Date(fecha);
  var fecha_hoy_ano = fecha_hoy.getYear();
  var fecha_nac_ano = fecha_nac.getYear();
  var edad = fecha_hoy_ano - fecha_nac_ano;
  Alert.alert("Tienes "+edad+" años");
  this.setState({
    age: edad
  });
}
setEmail = (text) => {
  console.log(text);
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    console.log("Email is Not Correct");
    this.setState({ email: text, errors: 'Este correo no es valido aún' })
    return false;
  }
  else {
    this.setState({ email: text, errors: 'Correo valido' })
    console.log("Email is Correct");
  }
}

  insert(name1, name2,surname1,surname2,country,city,phone,age,genre,email,user,password) {
    var query = 'INSERT INTO Students (\
      StudentName\
      ,StudentName2\
      ,StudentSurname\
      ,StudentSurname2\
      ,IDCountry\
      ,IDCity\
      ,StudentPhone\
      ,StudentAge\
      ,IDGenre\
      ,StudentEmail\
      ,StudentUser\
      ,StudentPassword\
      ,active) Values(?,?,?,?,?,?,?,?,?,?,?,?,1);';
    var params = [name1,name2,surname1,surname2,country,city,phone,age,genre,email,user,password];
    db.transaction((tx) => {
        tx.executeSql(query, params, (tx, results) => {
            console.log(results);
            Alert.alert("Excelente", "Felicidades! Puedes comenzar a estudiar!!! ");
        }, function (tx, err) {
            Alert.alert("Espera un momento", "Error al guardar en BD: "+err);
            return;
        });
    });
  }
  render() {
    var paises=[];
    var generos=[];
    this.populateCountries();
    this.populateGenre();
    this.state.countries.forEach(function (pais) {
      paises.push(
        <Picker.Item label={pais.DepartmentName} value={pais.IDDepartment} key={pais.IDDepartment}/>
      );
    }.bind(this));
    this.state.genres.forEach(function (genero) {
      generos.push(
        <Picker.Item label={genero.GenreName} value={genero.IDGenre} key={genero.IDGenre}/>
      );
    }.bind(this));
    return (
      <Container>
        <Header>
        <Body>
          <Button full danger
            onPress={() => {
              this.navigatetologin();
              }}
            >
              <Text>No aun</Text>
            </Button>
          </Body>
          <Right />
        </Header>
        <Text>Despues de haber ingresado tu respuesta en cada pregunta, presiona el icono <Icon active name='checkmark-circle' /> en el teclado</Text>
        <Content>
          <Form>
            <Label>Primer nombre(obligatorio)</Label>
            <Item rounded>
              <Input placeholder="" onChangeText={(val) => this.setState({ name1: val })} value={this.state.name1}/>
            </Item>
            <Label>Segundo nombre (opcional)</Label>
            <Item rounded>
              <Input placeholder="" onChangeText={(val) => this.setState({ name2: val })} value={this.state.name2}/>
            </Item>
            <Label>Primer apellido (obligatorio)</Label>
            <Item rounded>
              <Input placeholder="" onChangeText={(val) => this.setState({ surname1: val })} value={this.state.surname1}/>
            </Item>
            <Label>Segundo apellido (opcional)</Label>
            <Item rounded>
              <Input placeholder="" onChangeText={(val) => this.setState({ surname2: val })} value={this.state.surname2}/>
            </Item>
            <Item picker>
            <Label>Selecciona genero</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                placeholder="genero"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.genre}
                onValueChange={this.onValueChangeGenre.bind(this)}
              >
                {generos}
              </Picker>
            </Item>
            <Item picker>
            <Label>Selecciona Departamento</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                placeholder="pais"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.city}
                onValueChange={this.onValueChangeCountry.bind(this)}
              >
                {paises}
              </Picker>
            </Item>
            <Label>Celular (obligatorio)</Label>
            <Item rounded>
              <Input 
              placeholder="" 
              onChangeText={(val) => this.setState({ phone: val })} 
              value={this.state.phone}
              keyboardType={'phone-pad'}
              />
            </Item>
            <Label>Fecha de nacimiento (obligatorio)</Label>
            <DatePicker
                onDateChange={(date) => this.setAge(date)}
            />
            <Label>Correo (opcional)</Label>
            <Item rounded>
              <Input placeholder="" onChangeText={(val) => this.setEmail(val)} value={this.state.email}/>
            </Item>
            <Text>
              {this.state.errors}
            </Text>
            <Label>Ingresa tu usuario deseado (ejemplo: admin1)(obligatorio)</Label>
            <Item rounded>
              <Input onChangeText={(val) => this.setState({ user: val })} value={this.state.user}/>
            </Item>
            <Label>Ingresa tu nueva contraseña(obligatorio)</Label>
            <Item rounded>
              <Input onChangeText={(val) => this.setState({ password: val })} value={this.state.password}/>
            </Item>
          </Form>
        </Content>
        <Footer>
          <FooterTab>
            <Button full success
              onPress={() => {
                this.handleSave();
                }}
            >
              <Text>Registrate</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
  populateCountries(){
    var query = 'select IDDepartment,DepartmentName from Department where active=1';
    var params = [];
    var queryresults = new Array();
    db.transaction((tx) => {
      tx.executeSql(query, params, (tx, results) => {
          queryresults=results.rows._array;
          this.setState({ countries: queryresults })
      }, 
      function (tx, err) {
        Alert.alert(err);
      });
    });
  }
  populateGenre(){
    var query = 'select IDGenre,GenreName from Genre where active=1';
    var params = [];
    var queryresults = new Array();
    db.transaction((tx) => {
      tx.executeSql(query, params, (tx, results) => {
          queryresults=results.rows._array;
          this.setState({ genres: queryresults })
      }, 
      function (tx, err) {
        Alert.alert(err);
      });
    });
  }
}