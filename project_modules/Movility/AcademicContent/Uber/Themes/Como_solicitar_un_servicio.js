import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Uber_Como_pagar_un_servicio')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Como solicitar un servicio</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Ya iniciada la sesión se debes  seleccionar tu punto de partida y de destino eligiendo una ubicación en la lista o seleccionando uno de los resultados de búsqueda.
			{"\n"}{"\n"}
			Tip: para hacerlo de forma más rápida y eficiente, habilitá el GPS en tu teléfono.
			{"\n"}{"\n"}
			Una vez que hayás ingresado esa información, te aparecerá la ruta junto con la tarifa y la duración del viaje. Puedes deslizar a los lados para ver otras opciones de viaje.
			{"\n"}{"\n"}
			Cuando hayás encontrado el que más te convenga, tocá el botón ‘Solicitar’. Esperá a que uno de nuestros Socios Conductores acepte la solicitud, y cuando uno lo haya hecho, podrás ver su ubicación en el mapa y recibirás una notificación cuando esté cerca del punto de recogida.
			{"\n"}{"\n"}
			Ahora que ya sabes cómo usar Uber, solo te queda disfrutar del viaje.
			</Text>
			<Image source={require('../Assets/Como_pedir_uber.png')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}