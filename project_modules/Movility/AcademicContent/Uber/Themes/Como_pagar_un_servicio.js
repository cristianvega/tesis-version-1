import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Uber_Como_agregar_direcciones_en_Uber')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Como pagar un servicio</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Tarjeta de Credito{"\n"}
			Abre el menú en la aplicación de Uber.{"\n"}
			Selecciona la opción de Pago.{"\n"}
			Haz clic en Añadir forma de pago.{"\n"}
			Elige la opción Tarjeta de crédito.{"\n"}
			Saca tu tarjeta de crédito y escanéala con la cámara de tu celular o, si lo prefieres, inserta  los datos.{"\n"}
			Pago en efectivo.{"\n"}
			Abre el menú en la aplicación de Uber.{"\n"}
			Selecciona la opción de Pago.{"\n"}
			Haz clic en Añadir forma de pago.{"\n"}
			Elige la opción Pago en efectivo.{"\n"}
			</Text>
			<Image source={require('../Assets/output-onlinepngtools.png')} style={{height: 400, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
