import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Uber_Como_solicitar_un_servicio')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Que es Uber</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Uber es una aplicacion que se dedica al transporte de pasajeros conectando a clientes y conductores gracias a una aplicación gratuita para teléfonos móviles. 
			{"\n"}{"\n"}
			A traves de esta aplicación para móvil, el cliente tiene la posibilidad de ver qué coche de Uber está más cerca de casa y puede solicitarlo sin necesidad de bajar a la calle o tener que esperar a que aparezca.
			{"\n"}{"\n"}
			Además, derivado de lo anterior, casi no hay tiempos de espera. 
			{"\n"}{"\n"}Una vez hecha la solicitud el cliente tiene la certeza de que en como mucho 5 minutos tendrá el coche esperando en la puerta de casa. Vehículos que destacan por su limpieza y confort, ya que es obligatorio para los conductores de Uber que su vehículo no tenga más de cinco años de antigüedad. Por último cabe destacar también las tarifas. Claras, sencillas y sin sobrecostes ocultos. 
			{"\n"}{"\n"}El cliente paga sólo por el tiempo del trayecto y los kilómetros recorridos.
			</Text>
			<Image source={require('../Assets/uber_1.jpg')} style={{height: 360, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}