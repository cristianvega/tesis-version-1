import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('Movility_Picap_Como_solicitar_un_servicio_de_Picup')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Que es Picap</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Picap es una plataforma que permite a los ciudadanos desplazarse en una nueva modalidad de transporte, el mototaxismo. Picap te lleva a tu destino en motocicleta. Es decir su principal propósito es estar en esos momentos en los que necesitas evitar el tráfico para llegar a tiempo a alguna reunión.  {"\n"}{"\n"}
				</Text>
				<Image source={require('../Assets/picap.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
