import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetoEnd=()=>{
		this.props.navigation.navigate('ThemesMovility')
	}  
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Como agregar direcciones en Picap</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			CÓMO AGREGAR LA DIRECCIÓN DE TU TRABAJO O DE TU CASA A FAVORITOS{"\n"}{"\n"}
			Para guardar la dirección de tu casa o de tu trabajo, sigue estos pasos:{"\n"}
			1. Toca el icono del menú en la app{"\n"}
			2. Toca Configuración{"\n"}
			3. Toca Agregar la dirección de casa o Agregar la dirección del trabajo{"\n"}
			4. Ingresa la dirección de tu casa o de tu trabajo{"\n"}
			Las direcciones aparecerán en tu lista de favoritos en la sección Configuración de la cuenta.{"\n"}
			{"\n"}{"\n"}
			CÓMO ELIMINAR LA DIRECCIÓN DE TU TRABAJO O DE TU CASA DE FAVORITOS{"\n"}{"\n"}
			1. Toca el icono del menú en la app{"\n"}
			2. Toca Configuración{"\n"}
			3. Selecciona la dirección que quieres eliminar (trabajo o casa) y deslízala hacia la izquierda para completar la acción{"\n"}
			{"\n"}{"\n"}
			CÓMO AGREGAR UN DESTINO A LAS UBICACIONES GUARDADAS{"\n"}{"\n"}
			Después de llegar a un destino, verás el mensaje Guardar este destino en la información de inicio de la app. Para guardarlo, sigue estos pasos:{"\n"}
			1. Toca Agregar a Ubicaciones guardadas{"\n"}
			2. Escribe el nombre o apodo del lugar (p. ej., Casa de Joe o Tienda de mascotas){"\n"}
			3. Toca Guardar ubicación{"\n"}
			Nota: Solo podrás agregar un destino como una Ubicación guardada después de realizar un viaje hacia ese lugar.{"\n"}
			{"\n"}{"\n"}
			CÓMO ELIMINAR UNA UBICACIÓN GUARDADA{"\n"}{"\n"}
			Si ya no quieres que un destino figure entre las Ubicaciones guardadas, sigue estos pasos:{"\n"}
			1. Toca el icono del menú en la app{"\n"}
			2. Toca Configuración{"\n"}
			3. Toca Más Ubicaciones guardadas{"\n"}
			4. Toca la X junto al lugar que quieres eliminar{"\n"}
			5. Selecciona Eliminar Ubicación guardada{"\n"}
			</Text>
			<Image source={require('../Assets/como_agregar_direcciones.png')} style={{height: 300, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full success onPress={this.navigatetoEnd}>
					<Text>Final</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
