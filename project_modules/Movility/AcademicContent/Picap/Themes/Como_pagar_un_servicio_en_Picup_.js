import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('Movility_Picap_Como_agregar_direcciones_en_Picup')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como pagar un servicio en Picap</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Hay varias formas de pagar con efectivo o con tarjeta de crédito. Tú eliges. 
				{"\n"}{"\n"}
				La facilidad de la tarjeta{"\n"}
					Abre el menú en la aplicación de Picup.{"\n"}
					Selecciona la opción de Pago.{"\n"}
					Haz clic en Añadir forma de pago.{"\n"}
					Elige la opción Tarjeta de crédito.{"\n"}
					Saca tu tarjeta de crédito y escanéala con la cámara de tu celular o, si lo prefieres, inserta  los datos.{"\n"}
					{"\n"}{"\n"}
				Así de fácil. El pago con tu tarjeta de crédito es muy sencillo. {"\n"}
				Una vez hayas agregado tu tarjeta a la app de Picup, puedes comenzar a usarla  y conocer la estimación de tu tarifa, n Socio Conductor llegará por ti en unos minutos y podrá dejarte en tu destino.{"\n"}
				Hay algunas cosas que debes tener en cuenta al pagar con tu tarjeta de crédito:{"\n"}{"\n"}
				Una vez que hayas solicitado tu viaje, se hace una pre-autorización del cupo. Es posible que tu banco te envíe una notificación. Es una medida de verificación y no implicaningún cargo.{"\n"}
				Pago en efectivo  {"\n"}
				{"\n"}{"\n"}
				Si cuando estabas alistándote pensaste que prefieres pagar en efectivo, también se puede. Si ojeas y pasas tus dedos entre los billetes y crees que te alcanza, pero no estás seguro, la app te calcula el valor estimado del viaje. Abre la aplicación, ingresa a dónde quieres ir y así sabrás si tienes lo justo para pagar. Para poder usar la opción de pago en efectivo:
				{"\n"}{"\n"}
					Abre el menú en la aplicación de Picup.{"\n"}
					Selecciona la opción de Pago.{"\n"}
					Haz clic en Añadir forma de pago.{"\n"}
					Elige la opción Pago en efectivo.{"\n"}
				Listo!
				</Text>
				<Image source={require('../Assets/pago_picup.png')} style={{height: 230, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}