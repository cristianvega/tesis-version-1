import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Picap_Como_pagar_un_servicio_en_Picup_')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Como registrarse en Picap</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Para registrarte debes bajar la aplicación PICAP en : Android - Play Store o IOS - App Store. 
			{"\n"}{"\n"}
			Ingresa tus datos, lo puedes hacer con tus datos de facebook o correo electrónico. Necesitas una dirección de correo electrónico y un número de teléfono válidos. 
			{"\n"}{"\n"}
			Además, deberás ingresar una contraseña y aceptar nuestros Términos y condiciones, y el Aviso de privacidad.
			{"\n"}{"\n"}
			Proporciona tu nombre y apellido, número de teléfono e idioma de preferencia. Después de completar esta parte del proceso de registro, te enviaremos un SMS para verificar tu número de teléfono. Una vez que brindes esta información, te enviaremos un correo electrónico para confirmar el registro de la cuenta. Después de la confirmación, podrás usar la app para pedir viajes.
			</Text>
			<Image source={require('../Assets/picap_registro.png')} style={{height: 400, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
