import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('Movility_Picap_Como_registrarse_en_picup_')
		}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como solicitar un servicio de Picap</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				La dirección la pones en la pantalla de inicio justo encima del botón "solicitar"
				{"\n"}{"\n"}
				Oprimes en "dirección Picap", luego le das en solicitar y te aparece "agregar destino", allí escribes la dirección respectiva y luego solicitas el servicio.
				</Text>
				<Image source={require('../Assets/Servicio_picap.png')} style={{height: 400, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
