import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Cuper_Como_solicitar_un_servicio')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Como funciona Cuper</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			PASO 1{"\n"}
			Descarga Cuper en la play store,
			{"\n"}registra tus datos o ingresa con tus redes
			sociales.
			{"\n"}{"\n"}
			PASO 2{"\n"}
			Elige tu ubicación de inicio y el destino al cual te quieres dirigir, selecciona el servicio que deseas, verifica el precio y confirma tu servicio, en unos minutos llegaran por ti, así de fácil.
			{"\n"}{"\n"}PASO 3{"\n"}
			Verifica la placa de tu vehiculo, nombre condutor y disfruta el viaje en cuper, refiere amigos y gana dinero para tus proximos viajes.{"\n"}{"\n"}
			</Text>
			<Image source={require('../Assets/funcionamiento_cuper.jpg')} style={{height: 250, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
