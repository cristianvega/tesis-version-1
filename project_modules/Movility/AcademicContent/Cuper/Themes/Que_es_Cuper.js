import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Cuper_Como_funciona_Cuper')
		} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Que es Cuper</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Cuper es una aplicación que presta servicios de mototaxi, mensajería y auto privado. Cuper Opera actualmente bajo el sistema Android y se encuentra en desarrollo para la plataforma IOS.{"\n"}{"\n"}
			Ayuda a la gente a movilizarse por la ciudad, Realiza domicilios, entrega paquetes y gana dinero con tu propio horario.{"\n"}{"\n"}
			Ofrece además a quienes quieren conducir, ofrece una forma flexible de ganar dinero extra.{"\n"}{"\n"}
			Se debe descargar la aplicación Cuper Conductor y para trabajar con cuper, solo tienes que inscribirte, esperar la capacitación y la inspección de tu moto o carro. apenas pases nuestros filtros te guiaran  a cada paso y te notificaran cuando todo esté listo para empezar a ganar dinero.{"\n"}{"\n"}
			</Text>
			<Image source={require('../Assets/cuper.png')} style={{height: 360, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
