import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('Movility_Cuper_Como_pagar_un_servicio_Cuper')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como cambiar mi punto de Partida</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Si necesitas un punto de partida diferente, realiza lo siguiente:
				{"\n"}{"\n"}1. Toca en la etiqueta de punto de partida en el mapa.
				{"\n"}{"\n"}2. Ingresa una nueva dirección o edificio.
				{"\n"}{"\n"}3. Elige una de las ubicaciones sugeridas por la aplicación.
				{"\n"}{"\n"}También puedes cambiar tu punto de partida después de pedir un arrendamiento. Para ello, realiza lo siguiente:
				{"\n"}{"\n"}1. Toca Editar junto a tu punto de partida.
				{"\n"}{"\n"}2. Escribe una nueva dirección o arrastra tu marcador a cualquier ubicación del mapa dentro del círculo gris.
				{"\n"}{"\n"}3. Toca el botón Confirmar para completar tu pedido.
				{"\n"}{"\n"}Solo podrás ajustar el punto de partida una vez, y solo se puede mover dentro del círculo que se muestra en tu pantalla.
				</Text>
				<Image source={require('../Assets/punto_partida_cuper.jpg')} style={{height: 360, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
