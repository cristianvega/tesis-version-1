import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetoEnd=()=>{
		this.props.navigation.navigate('ThemesMovility')
	}  
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Como pagar un servicio Cuper</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			CÓMO AGREGAR UN MÉTODO DE PAGO{"\n"}{"\n"}
			1. Selecciona Wallet en el menú de la app.{"\n"}{"\n"}
			2. Toca Agregar método de pago.{"\n"}{"\n"}
			3. Escanea la tarjeta, ingresa los datos manualmente o agrega otro método de pago.{"\n"}{"\n"}

			CÓMO ESCANEAR UNA TARJETA DE CRÉDITO O DE DÉBITO{"\n"}{"\n"}
			1. Para escanear una tarjeta, toca el icono de la cámara. Puede que debas permitir que la app de Uber tenga acceso a la cámara de tu teléfono.{"\n"}{"\n"}
			2. Ubica la tarjeta en el centro de la pantalla del teléfono hasta que las 4 esquinas parpadeen en verde. Las tarjetas con letras y números en relieve suelen ser las más fáciles de escanear.{"\n"}{"\n"}
			3. Ingresa la fecha de vto. y el código de seguridad (CVV) de la tarjeta, y el código postal o de facturación.{"\n"}{"\n"}
			4. Toca GUARDAR.
			</Text>
			<Image source={require('../Assets/pago_cuper.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full success onPress={this.navigatetoEnd}>
				<Text>Final</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
