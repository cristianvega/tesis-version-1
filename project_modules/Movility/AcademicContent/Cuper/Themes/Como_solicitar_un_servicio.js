import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Cuper_Como_cambiar_mi_punto_de_Partida')
		} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Como solicitar un servicio</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			1. Ingresa tu dirección de destino en la casilla ¿A dónde vas? o toca el icono de acceso directo en la parte inferior de la pantalla. Los accesos directos incluyen los destinos recientes de tu historial de viajes o las Ubicaciones guardadas personalizadas que puedes establecer en la app
			{"\n"}{"\n"}
			2. Tu punto de partida predeterminado se establece según tu ubicación GPS actual. Si no llegan por ti en tu ubicación actual, toca tu punto de partida en el mapa y actualiza la dirección
			{"\n"}{"\n"}
			3. Toca Pedir. Es posible que tengas que confirmar tu punto de partida.
			{"\n"}{"\n"}
			Espera a que un conductor acepte tu pedido.
			{"\n"}{"\n"}
			4. Una vez que se aceptó tu pedido, verás la ubicación de tu conductor en el mapa, junto con un tiempo estimado de llegada al punto de partida.
			{"\n"}{"\n"}
			5. La app te notificará cuando el conductor esté cerca de tu punto de partida
			</Text>
			<Image source={require('../Assets/Como_solicitar_un_servicio_cuper.png')} style={{height: 400, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
