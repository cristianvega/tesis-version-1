import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesMovility')
		}  
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como cambiar el destino</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Tú eres el único que puede cambiar el destino una vez que el viaje fue iniciado. 
				{"\n"}{"\n"}Si no ingresaste bien la dirección, edítala desde tu celular para que nuestro sistema recalcule la tarifa y le notifiquemos al usuario conductor del cambio.
				{"\n"}{"\n"}Si bien no contamos con la función de paradas múltiples en un solo viaje, puedes coordinarlo con anticipación con el usuario conductor para evitar cualquier inconveniente y llegar tranquilo a tu destino.
				</Text>
				<Image source={require('../Assets/cambiar_destino.jpeg')} style={{height: 400, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full success onPress={this.navigatetoEnd}>
					<Text>Finalizar</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
