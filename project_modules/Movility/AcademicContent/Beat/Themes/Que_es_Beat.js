import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('Movility_Beat_Como_registrarse_en_Beat')
		} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Que es Beat</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Beat es una empresa dedicada al desarrollo de aplicaciones móviles para facilitar la interconexión de usuarios que buscan movilizarse dentro de ciudades.​ A través de la app Beat, los conductores registrados podrán recibir solicitudes de servicio de transporte de los usuarios pasajeros.{"\n"}{"\n"}
			Cada solicitud de viaje incluye el nombre, ubicación y otros datos del pasajero para que el conductor pueda identificarlo.{"\n"}{"\n"}
			Al igual que en otras aplicaciones similares, los conductores de Beat podrán calificar y publicar sus comentarios sobre su experiencia con el pasajero, así como acceder a las calificaciones realizadas por otros usuarios conductores de la aplicación Beat.{"\n"}{"\n"}
			Según aclaran desde sus términos y condiciones, la aplicación Beat «no es una empresa de taxi, transporte de pasajeros ni proporciona servicios de taxi«.{"\n"}{"\n"}
			El usuario conductor «únicamente es un conductor autorizado por Beat» tras previa previa evaluación y sólo a efectos de utilizar la aplicación.{"\n"}{"\n"}
			Cada conductor puede aceptar o rechazar libremente una solicitud de viaje.{"\n"}{"\n"}
			</Text>
			<Image source={require('../Assets/beat.png')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}