import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('Movility_Beat_Como_pagar_un_servicio_de_Beat')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como solicitar un servicio de Beat</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Abre la app
				{"\n"}{"\n"}Una vez descargada la app de Beat, ábrela desde tu celular verificando que el GPS esté encendido y tengas activados los datos móviles o el WiFi.
				{"\n"}{"\n"}Selecciona tu punto de origen y destino
				{"\n"}{"\n"}Ingresa la dirección de origen y destino de tu viaje. Si no encuentras alguna de las direcciones, puedes deslizar la pantalla y seleccionar el punto manualmente. 
				{"\n"}{"\n"}Verifica la tarifa
				{"\n"}{"\n"}La tarifa del viaje será calculada teniendo en cuenta la distancia y tiempo en llegar a tu destino. Puedes revisar el detalle de la tarifa haciendo click en ella y ten en cuenta que esta pueda variar en función de la demanda y hora.
				{"\n"}{"\n"}Presiona "Viaja con Beat"
				{"\n"}{"\n"}Cuando presiones "Viaja con Beat", la app te asignará al usuario conductor más cercano y este empezará a manejar hacia tu punto de origen para poder realizar el viaje.
				{"\n"}{"\n"}Verifica los datos del conductor y auto
				{"\n"}{"\n"}Desde la pantalla de la app podrás verificar los datos del conductor y del vehículo para que puedas encontrarlo en el punto de origen. No olvides confirmar su identidad antes de subir al auto e iniciar el viaje.
				{"\n"}{"\n"}Califica a tu conductor
				{"\n"}{"\n"}¡Llegaste a tu destino! Si tu pago fue en efectivo, podrás realizar el pago en este momento. Al bajar del auto, no olvides calificar a tu conductor. Tus comentarios son importantes para continuar ofreciéndote el mejor servicio.
				</Text>
				<Image source={require('../Assets/solicitu_de_viaje_Beat.jpg')} style={{height: 400, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
