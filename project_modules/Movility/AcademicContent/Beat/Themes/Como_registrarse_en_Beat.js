import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('Movility_Beat_Como_solicitar_un_servicio_de_Beat')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como registrarse en Beat</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Para crear una cuenta Beat, necesitas una dirección de correo electrónico y un número de teléfono válidos. Además, deberás ingresar una contraseña y aceptar nuestros Términos y condiciones, y el Aviso de privacidad.
				{"\n"}{"\n"}
				Proporciona tu nombre y apellido, número de teléfono e idioma de preferencia. Después de completar esta parte del proceso de registro, te enviaremos un SMS para verificar tu número de teléfono.
				{"\n"}{"\n"}
				A continuación, ingresa la información de pago. Agrega el número de tu tarjeta de crédito o de débito para permitir el cobro automático de las tarifas después de cada viaje. Ten en cuenta que no aceptamos tarjetas prepagas.
				{"\n"}{"\n"}
				Una vez que brindes esta información, te enviaremos un correo electrónico para confirmar el registro de la cuenta. Después de la confirmación, podrás usar la app para pedir viajes.
				</Text>
				<Image source={require('../Assets/Registro_Beat.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}