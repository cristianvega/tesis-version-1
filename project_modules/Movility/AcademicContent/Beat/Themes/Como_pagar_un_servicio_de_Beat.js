import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('Movility_Beat_Como_cambiar_el_destino_')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como pagar un servicio de Beat</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Para pagar en efectivo . Luego de ingresar tu destino y antes de pedir un viaje, verifica en la parte inferior izquierda de la pantalla que hayas escogido Efectivo como tu método de pago.
				{"\n"}{"\n"}Te recomendamos contar con el monto exacto a pagar cuando solicites el servicio o preguntarle al usuario conductor si tiene cambio. Si el viaje es cancelado, no tienes de qué preocuparte porque no realizaremos ningún cargo por penalidad en tu cuenta.
				{"\n"}{"\n"}Si quieres pagarlo con tarjeta , Luego de ingresar tu destino y antes de pedir un viaje, verifica en la parte inferior izquierda de la pantalla que hayas escogido tu método de pago preferido.
				{"\n"}{"\n"}Cuando solicites el servicio, cargaremos el monto exacto de la tarifa que apareció en la app y siempre en tu moneda. Es posible que algunos bancos realicen cargos adicionales.
				{"\n"}{"\n"}Si el viaje es cancelado, el monto que cargamos será liberado automáticamente por nuestro sistema y el tiempo que tomará la devolución depende de tu banco.
				</Text>
				<Image source={require('../Assets/pago_beat.jpg')} style={{height: 400, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}