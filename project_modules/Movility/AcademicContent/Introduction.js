import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
    this.props.navigation.navigate('Movility_Uber_Que_es_Uber')
  } 
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>Apps de movilidad</Title>
          </Body>
        </Header>
        <Content>
          <Text>
          La tendencia del elevado uso de aplicaciones móviles es fundamental a las diferentes edades y estratos, existen aplicaciones para solicitar transporte y para pedir domicilios, aspecto sustentado en la rentabilidad de estos servicios(transporte – taxi vs. bus y comida a domicilio).
          </Text>
          <Text>
          Fuente: https://m2m.com.co/movilidad/las-aplicaciones-moviles-cada-vez-juegan-un-rol-mas-preponderante-en-colombia/
          </Text>
          <Image source={require('../Images/movilidadytransporte.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
        </Content>
        <Footer>
          <FooterTab>
            <Button full warning onPress={this.navigatetocontentXX}>
              <Text>Siguiente</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}