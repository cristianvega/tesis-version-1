import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
navigatetocontent1=()=>{
    this.props.navigation.navigate('BasicC1')
} 
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>Que es Android</Title>
          </Body>
        </Header>
        <Content>
          <Text>
          Android es el nombre de un sistema operativo que se emplea en dispositivos móviles, por lo general con pantalla táctil. De este modo, es posible encontrar tabletas (tablets), teléfonos móviles (celulares) y relojes equipados con Android, aunque el software también se usa en automóviles, televisores y otras máquinas.
          </Text>
          <Image source={require('../Images/appsbasicas.png')} style={{height: 200, width: 360 , flex: 1}}/>
          <Text>
              Fuente: https://definicion.de/android/
          </Text>
        </Content>
        <Footer>
          <FooterTab>
            <Button full warning onPress={this.navigatetocontent1}>
              <Text>Siguiente</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}