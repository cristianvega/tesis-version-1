import React, { Component } from "react";
import { Container, Content, Card, CardItem, Text, Body, Button } from "native-base";
import {ProgressBarAndroid, Image} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
export default class CardItemBordered extends Component {
      //Rutas para todos los temas
    // Configuracion inicial del telefono
    navigatetoBasicintro=()=>{
      this.props.navigation.navigate('BasicIntro')
    } 
    // Como instalar aplicaciones desde Google Play
    // Realizar busquedas en el navegador
    // Acciones basicas del telefono
  render() {
    return (
      <Container>
        <Content padder>
        <Card>
            <CardItem header bordered>
              <Text>Configuracion inicial del teléfono</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoBasicintro}>
            <CardItem button bordered>
              <Body>
                <Image source={require('../Images/configuracion-inicial-android.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Como instalar aplicaciones desde Google Play</Text>
            </CardItem>
            <TouchableOpacity>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/Google-Playstore.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Realizar búsquedas desde el explorador</Text>
            </CardItem>
            <TouchableOpacity>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/exploradores-para-celular.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Acciones básicas para el manejo del telefono</Text>
            </CardItem>
            <TouchableOpacity>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/acciones-basicas-telefono.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}