import React, { Component } from "react";
import { Container, Content, Card, CardItem, Text, Body } from "native-base";
import {ProgressBarAndroid, Image} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
export default class CardItemBordered extends Component {
  navigatetoMovintro=()=>{
    this.props.navigation.navigate('MovIntro')
  } 
  navigatetoPicap=()=>{
		this.props.navigation.navigate('Movility_Picap_Que_es_Picap')
  } 
  navigatetoBeat=()=>{
    this.props.navigation.navigate('Movility_Beat_Que_es_Beat')
  } 
  navigatetoCuper=()=>{
		this.props.navigation.navigate('Movility_Cuper_Que_es_Cuper')
		} 
  render() {
    return (
      <Container>
        <Content padder>
        <Card>
            <CardItem header bordered>
              <Text>Uber</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoMovintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/uber.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Picap</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoPicap}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/picap.png')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Beat</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoBeat}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/beat.png')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Cuper</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoCuper}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/cuper.png')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}