import React, { Component } from "react";
import { Container, Content, Card, CardItem, Text, Body } from "native-base";
import {ProgressBarAndroid, Image} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
export default class CardItemBordered extends Component {
  navigatetoSocialintro=()=>{
    this.props.navigation.navigate('SocialIntro')
  } 
  navigatetoWhatsAppintro=()=>{
    this.props.navigation.navigate('SocialWO')
  } 
  navigatetoTwitterintro=()=>{
    this.props.navigation.navigate('SocialTO')
  } 
  navigatetoInstagramintro=()=>{
    this.props.navigation.navigate('SocialIO')
  } 
  render() {
    return (
      <Container>
        <Content padder>
        <Card>
            <CardItem header bordered>
              <Text>Facebook</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoSocialintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/facebook.png')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Whatsapp</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoWhatsAppintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/whatsapp.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Instagram</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoInstagramintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/instagram.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Twitter</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoTwitterintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/twitter.png')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}