import React, { Component } from "react";
import { Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Text, Body } from "native-base";
export default class CardTransparentExample extends Component {
  navigatetothemesocialmedia=()=>{
    this.props.navigation.navigate('ThemesSocialMedia')//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  navigatetothememovilty=()=>{
    this.props.navigation.navigate('ThemesMovility')//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  navigatetothemegoogle=()=>{
    this.props.navigation.navigate('ThemesGoogle')//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  navigatetothemebasic=()=>{
    this.props.navigation.navigate('ThemesBasic')//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  render() {
    return (
      <Container>
        <Content padder>
        <Card transparent>
            <CardItem header>
              <Text>Redes Sociales</Text>
            </CardItem>
            <CardItem button onPress={this.navigatetothemesocialmedia}>
              <Body>
              <Image source={require('../Images/imagen-redes-sociales.png')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
          </Card>
          <Card transparent>
            <CardItem header>
              <Text>Movilidad</Text>
            </CardItem>
            <CardItem button onPress={this.navigatetothememovilty}>
              <Body>
                <Image source={require('../Images/imagen-transporte.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
          </Card>
          <Card transparent>
            <CardItem header>
              <Text>Herramientas de Google</Text>
            </CardItem>
            <CardItem button onPress={this.navigatetothemegoogle}>
              <Body>
                <Image source={require('../Images/imagen-herramientas-de-google.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
          </Card>
          <Card transparent>
            <CardItem header>
              <Text>Manejo básico de celulares</Text>
            </CardItem>
            <CardItem button onPress={this.navigatetothemebasic}>
              <Body>
                <Image source={require('../Images/imagen-manejo-de-celular.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}