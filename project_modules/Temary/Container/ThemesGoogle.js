import React, { Component } from "react";
import { Container, Content, Card, CardItem, Text, Body } from "native-base";
import {ProgressBarAndroid, Image} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
export default class CardItemBordered extends Component {
  //Gmail
  navigatetoGoogleintro=()=>{
    this.props.navigation.navigate('GoogleIntro')
  } 
  //Maps
  navigatetoMapsintro=()=>{
    this.props.navigation.navigate('GoogleTools_Google_Maps_Que_es_Google_Maps')
  } 
  //Music
  navigatetoMusicintro=()=>{
    this.props.navigation.navigate('GoogleTools_Google_Music_Que_es_Google_Music')
  } 
  //Drive
  navigatetoDriveintro=()=>{
    this.props.navigation.navigate('GoogleD1')
  } 
  render() {
    return (
      <Container>
        <Content padder>
        <Card>
            <CardItem header bordered>
              <Text>Gmail</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoGoogleintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/gmail.png')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Maps</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoMapsintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/maps.jpeg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Google Music</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoMusicintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/google_music.jpg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Google Drive</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoDriveintro}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/drive.jpeg')} style={{height: 120, width: 300 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Progreso...   </Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Text>    50%</Text>
            </CardItem>
          </Card>
          
        </Content>
      </Container>
    );
  }
}