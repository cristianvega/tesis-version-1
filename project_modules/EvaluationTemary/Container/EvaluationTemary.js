import React, { Component } from "react";
import { Container, Content, Card, CardItem, Text, Body, Header, Title } from "native-base";
import {ProgressBarAndroid, Image} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
export default class CardItemBordered extends Component {
  constructor(){
    super()
    this.state = { 
        nombre: '', 
        apellido: '', 
        id: ''
    };
  }
  async UNSAFE_componentWillMount() {
    this.setState({ 
      nombre: this.props.navigation.state.params.StudentName,
      apellido: this.props.navigation.state.params.StudentSurname,
      id: this.props.navigation.state.params.IDStudent
    });
    console.log(this.props.navigation.state.params.IDStudent);
  }
  navigatetomodules=()=>{
    this.props.navigation.navigate('Modules',{IDStudent: this.props.navigation.state.params.IDStudent,StudentName: this.state.nombre,StudentSurname: this.state.apellido})//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  navigatetoFacebookEvaluation=()=>{
    this.props.navigation.navigate('FacebookEvaluation',{IDStudent: this.props.navigation.state.params.IDStudent,StudentName: this.state.nombre,StudentSurname: this.state.apellido})//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  }
  navigatetoNotes=()=>{
    this.props.navigation.navigate('ToNotes',{IDStudent: this.props.navigation.state.params.IDStudent,StudentName: this.state.nombre})//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  render() {
    return (
      <Container>
        <Header>
        <Title>Estudiante: {this.state.nombre+' '+this.state.apellido}</Title>
        </Header>
        <Content padder>
        <Card>
            <CardItem header bordered>
              <Text>Redes sociales</Text>
            </CardItem>
            <CardItem>
              <Body>
                    <Card>
                      <CardItem header bordered>
                        <Text>Facebook</Text>
                      </CardItem>
                      <TouchableOpacity onPress={this.navigatetoFacebookEvaluation}>
                      <CardItem bordered>
                        <Body>
                          <Image source={require('../Images/facebook.png')} style={{height: 50, width: 150 , flex: 1}}/>
                        </Body>
                      </CardItem>
                      </TouchableOpacity>
                      <CardItem footer bordered>
                          <Text>Progreso...   </Text>
                          <ProgressBarAndroid
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={0.5}
                          />
                          <Text>    50%</Text>
                      </CardItem>
                    </Card>
                    <Card>
                      <CardItem header bordered>
                        <Text>Whatsapp</Text>
                      </CardItem>
                      <TouchableOpacity>
                      <CardItem bordered>
                        <Body>
                        <Image source={require('../Images/whatsapp.jpg')} style={{height: 50, width: 150 , flex: 1}}/>
                        </Body>
                      </CardItem>
                      </TouchableOpacity>
                      <CardItem footer bordered>
                          <Text>Progreso...   </Text>
                          <ProgressBarAndroid
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={0.5}
                          />
                          <Text>    50%</Text>
                      </CardItem>
                    </Card>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Herramientas de Google</Text>
            </CardItem>
            <CardItem>
              <Body>
                    <Card>
                      <CardItem header bordered>
                        <Text>Gmail</Text>
                      </CardItem>
                      <TouchableOpacity>
                      <CardItem bordered>
                        <Body>
                        <Image source={require('../Images/gmail.png')} style={{height: 50, width: 150 , flex: 1}}/>
                        </Body>
                      </CardItem>
                      </TouchableOpacity>
                      <CardItem footer bordered>
                          <Text>Progreso...   </Text>
                          <ProgressBarAndroid
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={0.5}
                          />
                          <Text>    50%</Text>
                      </CardItem>
                    </Card>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Movilidad</Text>
            </CardItem>
            <CardItem>
              <Body>
                    <Card>
                      <CardItem header bordered>
                        <Text>Uber</Text>
                      </CardItem>
                      <TouchableOpacity>
                      <CardItem bordered>
                        <Body>
                          <Image source={require('../Images/uber.jpg')} style={{height: 50, width: 150 , flex: 1}}/>
                        </Body>
                      </CardItem>
                      </TouchableOpacity>
                      <CardItem footer bordered>
                          <Text>Progreso...   </Text>
                          <ProgressBarAndroid
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={0.5}
                          />
                          <Text>    50%</Text>
                      </CardItem>
                    </Card>
                    <Card>
                      <CardItem header bordered>
                        <Text>Picap</Text>
                      </CardItem>
                      <TouchableOpacity>
                      <CardItem bordered>
                        <Body>
                          <Image source={require('../Images/picap.png')} style={{height: 50, width: 150 , flex: 1}}/>
                        </Body>
                      </CardItem>
                      </TouchableOpacity>
                      <CardItem footer bordered>
                          <Text>Progreso...   </Text>
                          <ProgressBarAndroid
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={0.5}
                          />
                          <Text>    50%</Text>
                      </CardItem>
                    </Card>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Examina tus notas</Text>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoNotes}>
            <CardItem bordered>
              <Body>
                <Image source={require('../Images/uber.jpg')} style={{height: 50, width: 150 , flex: 1}}/>
              </Body>
            </CardItem>
            </TouchableOpacity>
            <CardItem footer bordered>
                <Text>Haz click en la imagen</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}