import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    textcenter:{
      width: '100%',
      textAlign: 'center'
    },
    content:{
      flex: 1,
      justifyContent: 'center'
    },
    button:{
      marginLeft:'15%'
    },
    body: {
      paddingVertical: 40
    },
    updates:{
      backgroundColor: 'white',
      fontSize:10,
      fontStyle: 'italic'
    },
    updatesfooter:{
      backgroundColor: 'white',
      height: '3%'
    }
  });