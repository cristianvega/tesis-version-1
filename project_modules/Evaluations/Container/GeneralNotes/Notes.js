import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Text, Icon, Spinner, ListItem, Separator  } from 'native-base';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import {ScrollView, View, Alert} from 'react-native';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase("tesis4.db");

export default class AnatomyExample extends Component {
    constructor() {
        super();
        this.state = {
            nombre: '', 
            id : '',
            loading: true, 
            dataQuery: [], 
            Query: ''
        }
    }
    
    async UNSAFE_componentWillMount() {
        this.setState({ 
            nombre: this.props.navigation.state.params.StudentName,
            id: this.props.navigation.state.params.IDStudent,
            loading: false
        });
        await this.realizarQuery();
    }
    realizarQuery() {
        
        var query = 'SELECT n.Note as Nota, t.ThemeName as Tema FROM EvaluationNotes n INNER JOIN Themes t ON t.IDTheme = n.IDTheme WHERE n.IDStudent='+this.props.navigation.state.params.IDStudent;
        console.log(query);
        var params = [];
        var nuevoarray = [];
        db.transaction(
            (tx) => {
            tx.executeSql(
                query, params, 
                (tx, results) => {
                if (results.rows._array.length > 0) {
                    nuevoarray= results.rows._array;
                    console.log(nuevoarray);
                    this.setState({
                        dataQuery: results.rows._array
                    });
                }
                                
                Alert.alert("Perfecto","He realizado la consulta");
            }, function (tx, err) {
                Alert.alert("Error","Existe un error: "+err);
            });
        });
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner color="green" />
                </View>
            );
        }
        const listItem = this.state.dataQuery.map((item) =>
        <View key={item.id}>
            <Collapse style={{ marginBottom: 10, marginTop: 10 }}>
                <CollapseHeader>
                    <Separator>
                        <View style={{ flexDirection: 'row' }}>
                            <Text>Tema: {item.Tema}</Text>
                        </View>
                    </Separator>
                </CollapseHeader>
                <CollapseBody>
                    <ListItem style={{ marginBottom: 3, marginTop: 3 }}>
                        <Text><Text> Nota : </Text>{item.Nota}</Text>
                    </ListItem>
                </CollapseBody>
            </Collapse>
        </View>
        );
        return (
        <Container>
            <Header>
            <Body>
                <Title>Notas de: {this.state.nombre}</Title>
            </Body>
            </Header>
            <Content>
            <ScrollView>
                    <View>
                        {listItem}
                    </View>
            </ScrollView>
            </Content>
        </Container>
        );
    }
}