import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Radio, Text, ListItem, Picker, Icon } from 'native-base';
import { Alert } from 'react-native';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase("tesis4.db");



export default class AnatomyExample extends Component {
    constructor() {
        super();
        this.state = {
            nombre: '', 
            apellido: '', 
            id: '',
            itemQ1: '1',
            itemQ2: '1',
            itemQ3: '1',
            itemQ4: '1',
            Q1: 0,
            Q2: 0,
            Q3: 0,
            Q4: 0
       }
    }
    async UNSAFE_componentWillMount() {
    this.setState({ 
        nombre: this.props.navigation.state.params.StudentName,
        apellido: this.props.navigation.state.params.StudentSurname,
        id: this.props.navigation.state.params.IDStudent
    })
    }
    subirrespuestas=()=>{
        const total=this.state.Q1+this.state.Q2+this.state.Q3+this.state.Q4;
        var query = 'update EvaluationNotes set Note='+total+' where IDTheme=1 and IDStudent='+this.state.id;
        var params = [];
        Alert.alert('Respuestas correctas: '+total);
        db.transaction((tx) => {
            tx.executeSql(query, params, (tx, results) => {
                console.log(results);
                // Alert.alert("Excelente", "Se ha registrado tu respuesta ");
            }, function (tx, err) {
                Alert.alert("Espera un momento", "Error al registrar tu respuesta: "+err);
                return;
            });
        });
    } 
    render() {
        return (
        <Container>
            <Header>
            <Body>
                <Title>Evaluacion de Facebook para {this.state.nombre+' '+this.state.apellido}</Title>
            </Body>
            </Header>
            <Content>
            <Text>De que color es el logo de Facebook?</Text>
                <ListItem>
                    <Left>
                    <Text>Amarillo</Text>
                    </Left>
                    <Right>
                    <Radio 
                       selected={this.state.itemQ1 == '1'}                       
                       onPress={() => this.setState({ 
                           Q1: 0,
                           itemQ1: '1' 
                       })}
                    />
                    </Right>
                </ListItem>
                <ListItem>
                    <Left>
                    <Text>Azul</Text>
                    </Left>
                    <Right>
                    <Radio
                       selected={this.state.itemQ1 == '2'}                       
                        onPress={() => this.setState({ 
                            Q1: 1,
                            itemQ1: '2' 
                        })}
                    />
                    </Right>
                </ListItem>
                <ListItem>
                    <Left>
                    <Text>Rojo</Text>
                    </Left>
                    <Right>
                    <Radio 
                       selected={this.state.itemQ1 == '3'}                       
                       onPress={() => this.setState({ 
                           Q1: 0,
                           itemQ1: '3' 
                       })}                    
                    />
                    </Right>
                </ListItem>
                <Text>Como se publica en facebook?</Text>
                <ListItem>
                    <Left>
                    <Text>Se abre Facebook y se escribe en la seccion de pensamientos</Text>
                    </Left>
                    <Right>
                    <Radio                     
                       selected={this.state.itemQ2 == '1'}                       
                       onPress={() => this.setState({ 
                           Q2: 0,
                           itemQ2: '1' 
                       })}                    
                    />
                    </Right>
                </ListItem>
                <ListItem>
                    <Left>
                    <Text>Se abre Facebook y se escribe en la seccion de paginas</Text>
                    </Left>
                    <Right>
                    <Radio 
                       selected={this.state.itemQ2 == '2'}                       
                       onPress={() => this.setState({ 
                           Q2: 0,
                           itemQ2: '2' 
                       })}                    
                    />
                    </Right>
                </ListItem>
                <ListItem>
                    <Left>
                    <Text>Se abre Facebook y se escribe en la seccion de publicaciones</Text>
                    </Left>
                    <Right>
                    <Radio 
                       selected={this.state.itemQ2 == '3'}                       
                       onPress={() => this.setState({ 
                           Q2: 1,
                           itemQ2: '3' 
                       })} 
                    />
                    </Right>
                </ListItem>
                <Text>Como se envia una solicitud de amistad?</Text>
                <ListItem>
                    <Left>
                    <Text>Buscando al amigo</Text>
                    </Left>
                    <Right>
                    <Radio 
                       selected={this.state.itemQ3 == '1'}                       
                       onPress={() => this.setState({ 
                           Q3: 0,
                           itemQ3: '1' 
                       })}                     
                    />
                    </Right>
                </ListItem>
                <ListItem>
                    <Left>
                    <Text>Buscando al amigo y seleccionando el boton añadir a amigos</Text>
                    </Left>
                    <Right>
                    <Radio 
                       selected={this.state.itemQ3 == '2'}                       
                       onPress={() => this.setState({ 
                           Q3: 1,
                           itemQ3: '2' 
                       })} 
                    />
                    </Right>
                </ListItem>
                <ListItem>
                    <Left>
                    <Text>Saliendo de Facebook</Text>
                    </Left>
                    <Right>
                    <Radio 
                       selected={this.state.itemQ3 == '3'}                       
                       onPress={() => this.setState({ 
                           Q3: 0,
                           itemQ3: '3' 
                       })}                       
                    />
                    </Right>
                </ListItem>
                <Text>Como se llama el creador de Facebook?</Text>
                <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                headerStyle={{ backgroundColor: "#b95dd3" }}
                headerBackButtonTextStyle={{ color: "#fff" }}
                headerTitleStyle={{ color: "#fff" }}
                selectedValue={this.state.itemQ4}
                //onValueChange={this.onValueChange.bind(this)}
                >
                <Picker.Item label="Nelson Mandela" onPress={() => this.setState({ itemQ4:'1', Q4: 0})} value="1" />
                <Picker.Item label="Mark Zuckerberg" onPress={() => this.setState({ itemQ4:'2', Q4: 1})} value="2" />
                <Picker.Item label="Bruce Wayne" onPress={() => this.setState({ itemQ4:'3', Q4: 0})} value="3" />
                <Picker.Item label="Silvester Stallone" onPress={() => this.setState({ itemQ4:'4', Q4: 0})} value="4" />
                <Picker.Item label="Neo" value="0" />
                </Picker>
            </Content>
            <Footer>
            <FooterTab>
                <Button full success
                    onPress={() => {
                        this.subirrespuestas();
                    }}
                >
                <Text>Subir respuestas</Text>
                </Button>
            </FooterTab>
            </Footer>
        </Container>
        );
    }
}