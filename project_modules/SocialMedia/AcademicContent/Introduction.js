import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
  navigatetocontent0=()=>{
    this.props.navigation.navigate('SocialF0')
  } 
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>Que son las redes sociales</Title>
          </Body>
        </Header>
        <Content>
          <Text>
          Las redes sociales son sitios de Internet formados por comunidades de individuos con intereses o actividades en común (como amistad, parentesco, trabajo) y que permiten el contacto entre estos, con el objetivo de comunicarse e intercambiar información.
          {"\n"}{"\n"}
          Una red social es una estructura social compuesta por un conjunto de usuarios (tales como individuos u organizaciones) que están relacionados de acuerdo a algún criterio (relación profesional, amistad, parentesco, etc.).
          {"\n"}{"\n"}
          El tipo de conexión representable en una red social es una relación diádica o lazo interpersonal.
          {"\n"}{"\n"}
          ​Las redes sociales se han convertido, en pocos años, en un fenómeno global, se expanden como sistemas abiertos en constante construcción de sí mismos, al igual que las personas que las utilizan.
          </Text>
          <Image source={require('../Images/introduccion.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
        </Content>
        <Footer>
          <FooterTab>
            <Button full warning onPress={this.navigatetocontent0}>
              <Text>Introduccion</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}