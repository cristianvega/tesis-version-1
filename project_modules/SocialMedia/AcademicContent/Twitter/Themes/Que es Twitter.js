import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentT1=()=>{
			this.props.navigation.navigate('SocialT1')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Que es Twitter</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Esta plataforma social, es un servicio de comunicación bidireccional con el que puedes compartir información de diverso tipo de una forma rápida, sencilla y gratuita.
				{"\n"}{"\n"}
				Es una red perfectamente estructurada para compartir experiencias y vivencias en el momento en que suceden.
				{"\n"}{"\n"}
				El tipo de contenido que se publica en Twitter es de diversa naturaleza: podemos encontrarnos mensajes personales, fotografías, infografías, información corporativa, noticias, eventos, descuentos, publicidad, etc.
				</Text>
				<Image source={require('../Assets/twitter.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentT1}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											