import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentT3=()=>{
			this.props.navigation.navigate('SocialT3')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Enviar Mensajes en Twitter</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				1.Abre la aplicación de Twitter.
				{"\n"}{"\n"}2.Abre el ícono del sobre. Se ubica en la extremo inferior derecho de la pantalla. 
				{"\n"}{"\n"}3.Selecciona el ícono de “nuevo mensaje”.
				{"\n"}{"\n"}4.Escribe un nombre de usuario.
				{"\n"}{"\n"}5.Selecciona su nombre de usuario. Su nombre aparecerá en el cuadro de texto.
				{"\n"}{"\n"}6.Selecciona "Siguiente." 
				{"\n"}{"\n"}7.Escribe el mensaje en el cuadro de texto. En el mensaje, también puedes agregar imágenes, GIFs o emoticones seleccionando el ícono adecuado.
				{"\n"}{"\n"}8.Selecciona "Enviar." Este botón se ubica a la derecha del cuadro de texto y no aparecerá hasta que hayas ingresado
				</Text>
				<Image source={require('../Assets/twitter.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentT3}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											