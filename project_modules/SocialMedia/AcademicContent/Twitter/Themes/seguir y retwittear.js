import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentT4=()=>{
			this.props.navigation.navigate('SocialT4')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Seguir y retwittear</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Se denomina Retweet a un Tweet que compartes públicamente con tus seguidores.Pasa el cursor sobre el Tweet que deseas compartir:
				{"\n"}{"\n"}
				Haz clic en el botón Retwittear.  
				{"\n"}{"\n"}
				Se abrirá una ventana emergente donde se muestra el Tweet que vas a retwittear.
				{"\n"}
				Haz clic en el botón Retwittear.
				{"\n"}{"\n"}
				El Tweet se compartirá con todos tus seguidores como un Retweet.
				{"\n"}
				Seguir desde un Tweet
				{"\n"}{"\n"}
				Busca un Tweet de la cuenta que quieres seguir.{"\n"}
				Pasa el cursor sobre su nombre.{"\n"}
				Haz clic en el botón Seguir.
				</Text>
				<Image source={require('../Assets/twitter.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentT4}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											