import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesSocialMedia')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Personalizar Twitter</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Inicia sesión en twitter.com o abre la aplicación de Twitter.
				{"\n"}{"\n"}Dirígete a tu perfil.Pulsa o haz clic en el botón Editar perfil para poder editar los siguientes elementos de tu cuenta:
				Imagen de encabezado (el tamaño recomendado es de 1500 x 500 píxeles)
				{"\n"}{"\n"}
				Imagen de perfil (el tamaño recomendado es de 400 x 400 píxeles)
				{"\n"}{"\n"}
				Nombre,Biografía (160 caracteres como máximo),Ubicación,Sitio web,Color de motivo,Fecha de nacimiento, Pulsa o haz clic en cualquiera de estas áreas para hacer cambios y Pulsa o haz clic en Guardar cambios. 
				</Text>
				<Image source={require('../Assets/twitter.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full success onPress={this.navigatetoEnd}>
					<Text>Final</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											