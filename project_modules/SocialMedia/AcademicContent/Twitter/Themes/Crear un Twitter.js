import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentT2=()=>{
			this.props.navigation.navigate('SocialT2')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Crear un Twitter</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Paso 1
				{"\n"}
				Inicia sesión en Twitter.
				{"\n"}{"\n"}
				Paso 2
				{"\n"}
				Redacta un nuevo tweet en la caja "¿Qué está pasando?".
				{"\n"}{"\n"}
				Paso 3
				{"\n"}
				Escribe el signo "#", seguido inmediatamente de la frase deseada hashtag o frase de palabras clave. Por ejemplo, #manzana o #encuesta. Utiliza tu propio hashtag o usa un hashtag relevante que veas en tweets de los demás.
				{"\n"}{"\n"}
				Paso 4
				{"\n"}
				Haz clic en "Tweet" para enviar tu tweet.
				</Text>
				<Image source={require('../Assets/twitter.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentT2}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											