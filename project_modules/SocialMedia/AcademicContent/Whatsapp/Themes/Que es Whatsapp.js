import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentW1=()=>{
		this.props.navigation.navigate('SocialW1')
		} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Que es Whatsapp?</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			WhatsApp es una aplicación de chat para teléfonos móviles de última generación, los llamados smartphones. 
			{"\n"}{"\n"}
			Sirve para enviar mensajes de texto y multimedia entre sus usuarios.
			{"\n"}{"\n"}Tiene un funcionamiento sencillo y su aprendizaje es muy fácil. 
			{"\n"}{"\n"}Todo el mundo puede empezar a enviar y recibir wasaps de inmediato. 
			{"\n"}{"\n"}Eliges qué quieres compartir, escribes (o no) un mensaje y pulsas el botón de enviar. 
			{"\n"}{"\n"}Para recibir, abres la aplicación y seleccionas la conversación.
			Imagen Pendiente...
			</Text>
			<Image source={require('../Assets/whatsapp-google-now--620x349.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentW1}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
											