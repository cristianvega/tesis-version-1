import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentW3=()=>{
			this.props.navigation.navigate('SocialW3')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Agregar contactos</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				WhatsApp reconoce los contactos que utilizan WhatsApp a través de la libreta de contactos de tu teléfono.
				{"\n"}{"\n"}Cómo saber qué contactos usan WhatsApp
				{"\n"}{"\n"}
				Abre WhatsApp.
				{"\n"}{"\n"}
				Ve a la pestaña de Chats.
				{"\n"}{"\n"}
				Pulsa en el ícono de nuevo chat .
				{"\n"}{"\n"}
				Cómo añadir a un contacto
				{"\n"}{"\n"}
				Abre WhatsApp.
				{"\n"}{"\n"}
				Ve a la pestaña de Chats.
				{"\n"}{"\n"}
				Pulsa en el ícono de nuevo chat > Nuevo contacto.
				{"\n"}{"\n"}
				Para añadir a un contacto con un número de otro país, debes añadir el número en el formato internacional completo:
				{"\n"}{"\n"}
				+[Código del país][Número de teléfono completo]
				</Text>
				<Image source={require('../Assets/Agregar-Contacto-WhatsApp.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentW3}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
