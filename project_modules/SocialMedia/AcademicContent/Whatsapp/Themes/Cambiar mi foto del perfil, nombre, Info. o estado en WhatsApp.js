import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentW5=()=>{
			this.props.navigation.navigate('SocialW5')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Cambiar mi foto del perfil, nombre, Info. o estado en WhatsApp</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Foto del perfil
				{"\n"}{"\n"}
				Ve a WhatsApp > botón de Menú > Ajustes.
				{"\n"}{"\n"}
				Toca tu foto del perfil > ícono de la Cámara.
				Selecciona una imagen de tu galería, haz una nueva con tu cámara o elimina la que tienes actualmente.
				{"\n"}{"\n"}
				Nombre
				{"\n"}{"\n"}
				Ve a WhatsApp > botón de Menú > Ajustes.
				Toca tu foto del perfil y después tu nombre.
				{"\n"}{"\n"}
				Importante: Este es el nombre que solo será mostrado para aquellos contactos que no tengan tu número de teléfono guardado en la libreta de contactos de su teléfono.
				Info. Imagen Pendiente...
				</Text>
				<Image source={require('../Assets/whatsapp-google-now--620x349.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentW5}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											