import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentW4=()=>{
			this.props.navigation.navigate('SocialW4')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Enviar Mensajes</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Paso 1:
				{"\n"}{"\n"}
				Pulsa sobre el comando Nuevo mensaje, ubicado en la parte superior de la pantalla. Busca a la persona a la cual deseas enviarle el mensaje y presiona su nombre.
				{"\n"}{"\n"}
				Paso 2:

				Se abrirá la ventana de chat. En el espacio en blanco escribe el mensaje que deseas enviar.
				{"\n"}{"\n"}
				Paso 3:

				Si quieres, puedes añadir emoticonos pulsando sobre la carita. Allí se desplegará un menú con todas las opciones que tienes. Pulsa sobre el que más te guste.
				{"\n"}{"\n"}
				Paso 4:

				Pulsa sobre el ícono con forma de flecha y listo. El mensaje será enviado.
				</Text>
				<Image source={require('../Assets/whatsapp-google-now--620x349.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentW4}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											