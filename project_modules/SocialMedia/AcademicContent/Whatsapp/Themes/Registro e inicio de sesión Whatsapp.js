import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentW2=()=>{
			this.props.navigation.navigate('SocialW2')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Registro e inicio de sesión Whatsapp</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Primero, tienes que instalar la app. 
				{"\n"}{"\n"}Abre Google Play Store,Dentro de ella, busca WhatsApp  y escoge para instalarlo. 
				{"\n"}{"\n"}Luego abrir la aplicación. 
				{"\n"}{"\n"}Al entrar,aparecerá una ventana en la que se te solicita leer y aceptar las condiciones de uso y la política de privacidad. 
				{"\n"}{"\n"}Tienes que pulsar en Aceptar y continuar.Se deben conceder determinados permisos a la aplicación.
				{"\n"}{"\n"}Confirmar el teléfono que se va a vincular. 
				{"\n"}{"\n"}Introduce el número del que estás utilizando, ya que se enviará un SMS con un código con este cod ingresas a la aplicación. Ya esta listo!
				</Text>
				<Image source={require('../Assets/Verificar-cuenta-con-SMS-para-iniciar-sesion-en-Whatsapp.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentW2}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											