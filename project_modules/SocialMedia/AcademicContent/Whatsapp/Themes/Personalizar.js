import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesSocialMedia')
			}  
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Personalizar</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				1.Cambiar el fondo de cada chat.Por defecto, la app incluye algunos fondos predefinidos, aunque es posible elegir cualquier imagen de la galería como fondo de chat.
				{"\n"}{"\n"}
				2.Cambia el tamaño de la fuente. Si crees que el tamaño del texto de los mensajes de WhatsApp es demasiado pequeño, o demasiado grande, puedes cambiarlo fácilmente desde Ajustes/Chats/Tamaño de fuente.
				{"\n"}{"\n"}
				Activa o desactiva los sonidos del chat.WhatsApp también incluye efectos de sonido estos sonidos pueden ser activados o desactivados en dentro de la sección “Notificaciones”.
				</Text>
				<Image source={require('../Assets/como-personalizar-las-notificaciones-de-WhatsApp.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full success onPress={this.navigatetoEnd}>
					<Text>Final</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											