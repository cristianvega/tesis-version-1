import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentF4=()=>{
			this.props.navigation.navigate('SocialF4')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Mensajeria</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Abre Facebook Messenger. {"\n"}{"\n"}
				El ícono de la aplicación que se parece a un rayo blanco dentro de una burbuja de diálogo de color azul. {"\n"}{"\n"}
				Se abrirá la última pestaña de Facebook Messenger.{"\n"}{"\n"}
				Si te pide, primero ingresa el número de teléfono y contraseña.
				{"\n"}{"\n"}Selecciona el nombre de un amigo sugerido o escribe el nombre de un amigo en la barra de búsqueda en la parte superior de la pantalla y luego selecciona el perfil cuando aparezca debajo de la barra de búsqueda.
				{"\n"}{"\n"}Escribe el mensaje y luego selecciona la flecha "enviar" en el lado derecho del cuadro de texto. 
				Imagen Pendiente...
				</Text>
				<Image source={require('../Assets/Facebook1-730x395.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentF4}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											