import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentF1=()=>{
		this.props.navigation.navigate('SocialF1')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Que es Facebook?</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Básicamente, Facebook es una red social cuyo fin es la construcción de una lista de contactos con los que compartir, cada día, publicaciones en forma de texto e imágenes.
				{"\n"}{"\n"}
				Es una red social gratuita que permite a los usuarios interconectarse para interactuar y compartir contenidos a través de internet. 
				{"\n"}{"\n"}
				Facebook puede ser usado tanto por usuarios particulares, que lo utilizan para estar en contacto con sus amistades, publicar textos, fotos, videos, etc., como por empresas, marcas o celebridades. 
				{"\n"}{"\n"}
				</Text>
				<Image source={require('../Assets/Facebook1-730x395.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentF1}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}