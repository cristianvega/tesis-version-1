import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesSocialMedia')
			}  
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Sugerencias de Amistad</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Facebook creará una lista de amigos recomendados para ti.{"\n"}{"\n"}
				Esta es más precisa cuando ya has añadido algunos amigos, pero puedes ver sugerencias de amistad en cualquier momento.
				{"\n"}{"\n"} En Computadora. Haz clic en la pestaña de tu nombre, haz clic en Amigos debajo de la foto de portada, haz clic en ＋ Buscar amigos y haz clic en Añadir a mis amigos al lado de cada amigo al que desees añadir. 
				{"\n"}{"\n"} En Móvil. Pulsa ☰, pulsa Amigos, pulsa la pestaña Sugerencias y pulsa Añadir a mis amigos al lado de cada amigo al que desees añadir 
				</Text>
				<Image source={require('../Assets/v4-728px-Use-Facebook-Step-11-Version-2.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full success onPress={this.navigatetoEnd}>
					<Text>Final</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}