import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentF3=()=>{
			this.props.navigation.navigate('SocialF3')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Añadir amigos</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				1. Selecciona la barra de búsqueda. Haz clic en la barra de búsqueda que está en la parte superior de la página o de la pantalla.
				Esto colocará el cursor en la barra de búsqueda y hará que aparezca el teclado de pantalla si usas un dispositivo móvil. 
				{"\n"}{"\n"}
				2. Ingresa el nombre de un amigo. Escribe el nombre de la persona a la que desees añadir como amigo en Facebook,
				luego haz clic en el nombre que acabas de escribir cuando aparezca. haz clic en Añadir a mis amigos. Hacerlo enviará una solicitud de amistad a la persona si la acepta, ya seran amigos.
				{"\n"}{"\n"}
				</Text>
				<Image source={require('../Assets/v4-728px-Use-Facebook-Step-10-Version-2.jpg')} style={{height: 200, width: 360}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentF3}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											