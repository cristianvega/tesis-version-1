import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentF6=()=>{
		this.props.navigation.navigate('SocialF6')
		} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Registro e inicio de sesión</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Debes tener al menos 13 años para crear una cuenta de Facebook.
			{"\n"}{"\n"}
			1.Ve a www.facebook.com/r.php en el navegador  y/o buscalo con google.{"\n"}{"\n"}
			2.Escribe tu nombre,correo electrónico o No de teléfono celular, contraseña, fecha de nacimiento y sexo. {"\n"}{"\n"}
			3. Haz clic en Registrarte. {"\n"}{"\n"}
			4. Para terminar de crear la cuenta, debes confirmar tu correo electrónico o número de teléfono celular.{"\n"}{"\n"}
			Para iniciar sesión en tu cuenta de Facebook en una computadora: Ve a facebook.com. {"\n"}{"\n"}
			En la parte superior,escribe tu correo electronico ,tu Contraseña y da clic en iniciar sesión.{"\n"}{"\n"}
			Imagen Pendiente...
			</Text>
			<Image source={require('../Assets/Facebook1-730x395.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentF6}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}