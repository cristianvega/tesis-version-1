import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentF2=()=>{
		this.props.navigation.navigate('SocialF2')
		} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Agregar Amigos</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Encuentra a la persona que quieras añadir. 
			{"\n"}{"\n"}
			Puedes usar la barra de búsqueda en la parte superior de la página de FB para buscar a cualquiera que use FB ya sea por nombre o correo electrónico. 
			{"\n"}{"\n"}
			Haz clic en el resultado para abrir el perfil de esa persona. 
			{"\n"}{"\n"}
			Ve de dónde lo conoces. 
			{"\n"}{"\n"}
			Cuando visites el perfil de una persona, puedes ver si comparten amigos en común. 
			{"\n"}{"\n"}
			Esto puede ser útil para recordar de dónde lo conoces. 
			{"\n"}{"\n"}
			Haz clic en el botón de Agregar a amigos. 
			{"\n"}{"\n"}
			Al hacer clic en el botón de Agregar a amigos, este cambiará a un botón de Solicitud enviada.
			</Text>
			<Image source={require('../Assets/amigos-facebook-correo.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentF2}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
											