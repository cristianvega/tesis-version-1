import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentF5=()=>{
			this.props.navigation.navigate('SocialF5')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Publicación de contenidos</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				En la parte superior de la sección de noticias o la biografía, haz clic en Publicación.{"\n"}{"\n"}
				Agrega una actualización de texto o haz clic en el tipo de publicación que quieras compartir (por ejemplo, Foto/video o Sentimiento/actividad.
				Selecciona dónde quieres compartir la publicación. {"\n"}{"\n"}
				Si solo agregas una foto, un video o texto con un fondo como publicación, también tendrás la opción de compartirlo en tu historia. 
				Haz clic para seleccionar Sección de noticias, Tu historia o ambas opciones y luego da Haz clic en Compartir.{"\n"}{"\n"}
				Imagen Pendiente...
				</Text>
				<Image source={require('../Assets/Facebook1-730x395.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentF5}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											