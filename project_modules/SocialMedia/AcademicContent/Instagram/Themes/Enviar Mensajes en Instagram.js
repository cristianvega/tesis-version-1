import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentI3=()=>{
			this.props.navigation.navigate('SocialI3')
			}
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Enviar Mensajes en Instagram</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Para enviar un mensaje mediante Instagram Direct:
				{"\n"}{"\n"}
				Toca en la parte superior derecha o desliza el dedo hacia la izquierda en cualquier lugar del feed.
				{"\n"}{"\n"}
				Toca en la parte superior derecha.
				{"\n"}{"\n"}
				Selecciona las personas a las que quieras enviar un mensaje y toca Chat.
				{"\n"}{"\n"}
				Escribe un mensaje. También puedes tocar para tomar una foto o grabar un video temporal y enviarlos, o para seleccionar una foto o un video de la biblioteca.
				{"\n"}{"\n"}
				Toca Enviar.
				</Text>
				<Image source={require('../Assets/formulario-de-registro-de-instagram.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentI3}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											