import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesSocialMedia')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Seguir y aceptar contactos Instagramam</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				1.Abre la aplicación Instagram en tu teléfono.
				{"\n"}{"\n"}
				2.Ve a tu muro de actualizaciones. Es el botón que está justo a la derecha de tu icono de cámara. Es posible que sobre este icono aparezca otro icono pequeño de color naranja. Esto indicará el número de comentarios y de solicitudes de seguidores que tienes.
				{"\n"}{"\n"}
				3.Pulsa el nombre de usuario de la persona que deseas aprobar.
				{"\n"}{"\n"}
				4.Para aprobar la solicitud, pulsa la marca de verificación de color verde que está en la parte superior de la página de perfil. Si prefieres negar la solicitud, pulsa la X de color rojo
				Imagen Pendiente...
				</Text>
				<Image source={require('../Assets/formulario-de-registro-de-instagram.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full success onPress={this.navigatetoEnd}>
					<Text>Final</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											