import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('GoogleTools_Google_Music_Transmitir_música')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Agregar tu colección personal</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Puedes almacenar hasta 50,000 canciones propias de forma gratuita.
				{"\n"}{"\n"}
				Cargar música a la nube: Para cargar música a la nube, puedes utilizar el Administrador de Música o Google Play Música para Chrome. El Administrador de Música está disponible para Mac, PC y Linux. Si utilizas un navegador Chrome o una Chromebook, te recomendamos usar Google Play Música para Chrome.
				{"\n"}{"\n"}
				Transferir archivos a un dispositivo específico: Para transferir archivos de forma directa a un dispositivo específico, sigue las instrucciones sobre cómo transferir música de una computadora a un dispositivo.
				</Text>
				<Image source={require('../Assets/Agregar_tu_coleccion_personal.jpg')} style={{height: 300, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
