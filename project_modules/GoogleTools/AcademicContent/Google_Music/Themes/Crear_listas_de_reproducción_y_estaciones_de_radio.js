import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('GoogleTools_Google_Music_Cómo_configurar_la_app_de_Google_Play_Música')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Crear listas de reproducción y estaciones de radio</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Puedes crear listas de reproducción e iniciar estaciones de radio (en los países disponibles) en función de tus artistas favoritos o canciones de la biblioteca. 
				{"\n"}{"\n"}
				Si te suscribes a Google Play Música, no verás ningún anuncio ni tendrás límites de omisiones.
				</Text>
				<Image source={require('../Assets/Crear_listas_de_reproduccion_y_estaciones_de_radio.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
