import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('GoogleTools_Google_Music_Crear_listas_de_reproducción_y_estaciones_de_radio')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Que es Google Music</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Google Play Music es una aplicación que ofrece una radio gratuita con anuncios que se adapta a lo que se esta haciendo, a cómo te sientes o a lo que te apetece escuchar. 
				{"\n"}{"\n"}
				Empieza a escuchar al instante emisoras de radio basadas en canciones, artistas o álbumes, o haz búsquedas por género, estado de ánimo, actividad, década y mucho más. Lleva siempre contigo tu colección musical: puedes subir hasta 50.000 canciones para escucharlas en Android, en iOS o en la Web de forma gratuita.
				</Text>
				<Image source={require('../Assets/Que_es_Google_Music.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}