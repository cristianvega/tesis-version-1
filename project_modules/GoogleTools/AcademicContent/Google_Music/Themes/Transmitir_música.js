import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesGoogle')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Transmitir música</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Sin suscripción a Google Play Música en Estados Unidos: Puedes transmitir estaciones de radio gratuitas según tu estado de ánimo, actividad o música popular favorita. Puedes omitir hasta seis canciones por hora. Obtén más información sobre la radio gratuita.
				{"\n"}{"\n"}
				Con suscripción a Google Play Música: Puedes transmitir hasta treinta millones de canciones del catálogo de Google Play sin anuncios. También puedes omitir todas las canciones que quieras y escuchar música sin conexión. Obtén más información sobre cómo subscribirse a Google Play Música.
				</Text>
				<Image source={require('../Assets/Trasmitir_musica.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button success onPress={this.navigatetoEnd}>
					<Text>Finalizar</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
