import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('GoogleTools_Google_Music_Agregar_tu_colección_personal')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Cómo configurar la app de Google Play Música</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				La primera vez que abras la app de Google Play Música en un dispositivo móvil o una computadora, podrás elegir qué cuenta usar. Después de acceder a una cuenta, se agregará a la biblioteca la música que tengas almacenada en tu dispositivo, junto con la música que hayas subido o comprado en Google Play con tu cuenta.
				{"\n"}{"\n"}
				En Android TV, la aplicación Google Play Música usará la cuenta asociada con el dispositivo.
				{"\n"}{"\n"}
				Nota: Si no accedes a una cuenta de Google, solo podrás escuchar música que hayas copiado al dispositivo desde una computadora.
				</Text>
				<Image source={require('../Assets/Como_configurar_la_app_de Google_Play_Musica.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}