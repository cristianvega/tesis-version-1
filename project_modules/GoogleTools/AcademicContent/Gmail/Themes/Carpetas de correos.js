import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentNumeroX=()=>{
		this.props.navigation.navigate('GoogleC5')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Carpetas de correos</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Crear etiquetas para organizar Gmail
				{"\n"}{"\n"}
				Cuando guardes tus correos electrónicos, puedes asignarles etiquetas. Añádeles todas las que quieras. 
				{"\n"}{"\n"}
				Nota: Las etiquetas y las carpetas no son lo mismo. Si eliminas un mensaje, se borrará de todas las etiquetas que tenga asignadas y de tu bandeja de entrada.
				{"\n"}{"\n"}
				Ve a Gmail en un ordenador.{"\n"}{"\n"}
				Desplázate hacia abajo por el lateral izquierdo y haz clic en Más.{"\n"}{"\n"}
				Haz clic en Nueva etiqueta.{"\n"}{"\n"}
				Ponle un nombre.{"\n"}{"\n"}
				Haz clic en Crear.
				</Text>
				<Image source={require('../Assets/gmail.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentNumeroX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											