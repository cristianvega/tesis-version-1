import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentNumeroX=()=>{
	this.props.navigation.navigate('GoogleC2')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Que es Gmail</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Gmail es el servicio de correo electrónico de Google.{"\n"}{"\n"}
			Además del buzón de correo electrónico, puedes organizar tu correo mediante etiquetas.{"\n"}{"\n"}
			Permite centralizar el correo organizando las diferentes cuentas de correo, incluidas las de diferentes proveedores de correo electrónico, sin la necesidad de una aplicación de escritorio para poder hacerlo.{"\n"}{"\n"}
			A través de los correos prioritarios, Gmail ordenará tu bandeja de entrada según la importancia que des a esos mensajes y también con tu historial de emails.{"\n"}{"\n"}
			Así, Gmail podrá hacer los balances necesarios 
			</Text>
			<Image source={require('../Assets/gmail.png')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button info full onPress={this.navigatetocontentNumeroX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
											