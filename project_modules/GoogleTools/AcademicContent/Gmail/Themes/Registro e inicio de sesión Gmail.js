import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentNumeroX=()=>{
		this.props.navigation.navigate('GoogleC3')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Registro e inicio de sesión Gmail</Title>
				</Body>
			</Header>
			<Content>
				<Text>
					Accede a gmail.com en el ordenador.{"\n"}{"\n"}
					Escribe la dirección de correo de tu cuenta de Google o tu número de teléfono y la contraseña.{"\n"}{"\n"}
					Si ves datos ya rellenados pero quieres iniciar sesión en otra cuenta, haz clic en Usar otra cuenta.{"\n"}{"\n"}
					Si ves una página de descripción de Gmail en vez de la de inicio, haz clic en Iniciar sesión (arriba a la derecha).No podrás obtener una dirección de Gmail si el nombre de usuario que solicitaste: Ya está en uso.Es muy similar a un nombre de usuario existente (por ejemplo, si ejemplo@gmail.com ya existe{"\n"}{"\n"}
				</Text>
				<Image source={require('../Assets/gmail.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentNumeroX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											