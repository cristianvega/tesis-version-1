import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentNumeroX=()=>{
	this.props.navigation.navigate('GoogleC4')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Envio de mensajes en Gmail</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Escribir un correo {"\n"}{"\n"}
			Ve a Gmail en un ordenador.{"\n"}{"\n"}
			Arriba a la izquierda, haz clic en Redactar Redactar.{"\n"}{"\n"}
			En el campo "Para", añade a los destinatarios. {"\n"}{"\n"}
			Si quieres, también puedes añadir destinatarios en los campos "Cc" y "Cco".{"\n"}{"\n"}
			Escribe el asunto del correo.{"\n"}{"\n"}
			Redacta el mensaje.{"\n"}{"\n"}
			Al final de la página, haz clic en Enviar.{"\n"}{"\n"}
			</Text>
			<Image source={require('../Assets/gmail.png')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentNumeroX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
											