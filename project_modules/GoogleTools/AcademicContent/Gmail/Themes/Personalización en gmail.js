import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
		this.props.navigation.navigate('ThemesGoogle')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Personalización en gmail</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Temas de Gmail En tu computadora, abre Gmail. 
				{"\n"}{"\n"}En la esquina superior derecha, haz clic en Configuración .
				{"\n"}{"\n"}Haz clic en Temas.Haz clic en un tema. 
				{"\n"}{"\n"}Para hacer cambios en algunos temas, haz clic en Fondo de texto , Viñeta o Desenfoque.
				{"\n"}{"\n"}Haz clic en Guardar.Cómo usar una de tus fotos como fondo Para comenzar, asegúrate de haber subido la imagen que deseas usar en un álbum nuevo o existente, en photos.haz clic en Mis fotos.Elige una foto.Haz clic en Seleccionar.Cerca de la parte inferior, para hacer cambios en el tema, haz clic en guardar.
				</Text>
				<Image source={require('../Assets/gmail.png')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button success onPress={this.navigatetoEnd}>
					<Text>Finalizar</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
											