import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentXX=()=>{
		this.props.navigation.navigate('GoogleTools_Google_Maps_Cómo_establecer_tu_dirección_particular_o_de_trabajo')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Que es Google Maps</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Google Maps es el nombre de una aplicación desarrollada por Google que se encarga de ofrecer a los usuarios toda la información que necesiten sobre su ubicación actual, como también la de cualquier dirección específica, así como el trazado de recorridos para llegar al lugar que estos deseen desde donde se encuentran.
			{"\n"}{"\n"}
			Una app que aprovecha la conexión GPS de los celulare y tablets y la combina con el seguimiento de los mapas y los callejeros que realiza Google para dar a los consumidores una herramienta de lo más eficaz. Gracias a esto, no es necesario comprar mapas ni tampoco preguntar por direcciones, puedes buscarlas directamente en tu teléfono móvil y, además, saber por dónde ir para llegar en el menor tiempo posible.{"\n"}{"\n"}
			</Text>
			<Image source={require('../Assets/Inicial_que_es_Maps.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentXX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}