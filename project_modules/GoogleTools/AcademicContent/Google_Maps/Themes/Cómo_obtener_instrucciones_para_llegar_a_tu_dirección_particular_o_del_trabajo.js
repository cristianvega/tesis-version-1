import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('GoogleTools_Google_Maps_Cómo_cambiar_tu_dirección_particular_o_la_del_trabajo')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Cómo obtener instrucciones para llegar a tu dirección particular o del trabajo</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				1    Abre Google Maps y asegúrate de haber accedido a tu cuenta.{"\n"}{"\n"}
				2    En el cuadro de búsqueda, escribe dirección particular o dirección de trabajo.{"\n"}{"\n"}
				3    Haz clic en Cómo llegar Cómo llegar.{"\n"}{"\n"}
				4    Elige En automóvil, Transporte público, A pie o En bicicleta.{"\n"}{"\n"}

				Sugerencia: También puedes escribir dirección particular o dirección de trabajo en los campos de las instrucciones sobre cómo llegar para utilizar alguna de estas opciones como punto de partida o destino.{"\n"}{"\n"}
				</Text>
				<Text>
					ContenidosGuardados/GoogleTools/Google_Maps/Cómo_obtener_instrucciones_para_llegar_a_tu_dirección_particular_o_del_trabajo/Cómo obtener instrucciones para llegar a tu dirección particular o del trabajo.jpg
				</Text>
				<Image source={require('../Assets/Como_obtener_instrucciones_para_llegar_a_tu_direccion_particular_o_del_trabajo.jpg')} style={{height: 300, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}