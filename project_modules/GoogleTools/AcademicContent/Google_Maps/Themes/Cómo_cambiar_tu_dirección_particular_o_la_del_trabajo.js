import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('GoogleTools_Google_Maps_Cómo_solucionar_problemas_para_ver_tu_dirección_particular_y_del_trabajo_en_Maps')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Cómo cambiar tu dirección particular o la del trabajo</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				1    Abre Google Maps y asegúrate de haber accedido a tu cuenta.{"\n"}{"\n"}
				2    En el cuadro de búsqueda, escribe dirección particular o dirección del trabajo.{"\n"}{"\n"}
				3    Haz clic en Editar ubicado junto a la dirección que deseas modificar. {"\n"}{"\n"}
				4    Escribe la nueva dirección y, luego, haz clic Guardar.{"\n"}{"\n"}

				</Text>
				<Image source={require('../Assets/Como_cambiar_tu_direccion_particular_o_la_del_trabajo.jpg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}