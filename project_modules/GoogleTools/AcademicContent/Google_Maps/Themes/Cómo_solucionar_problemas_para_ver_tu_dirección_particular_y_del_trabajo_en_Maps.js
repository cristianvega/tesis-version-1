import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesGoogle')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Cómo solucionar problemas para ver tu dirección particular y del trabajo en Maps</Title>
				</Body>
			</Header>
			<Content>
				<Text>
					Para utilizar tu dirección particular y del trabajo cuando realizas una búsqueda o utilizas las instrucciones sobre cómo llegar, debes activar la opción "Actividad web y de apps". Si no ves las opciones "dirección particular" ni "trabajo" en Maps, obtén información sobre cómo activar la Actividad web y de aplicaciones.
				</Text>
				<Image source={require('../Assets/Como_solucionar_problemas_para_ver_tu_direccion_particular_y_del_trabajo_en_Maps.jpg')} style={{height: 210, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button success onPress={this.navigatetoEnd}>
					<Text>Finalizar</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}