import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentXX=()=>{
			this.props.navigation.navigate('GoogleTools_Google_Maps_Cómo_obtener_instrucciones_para_llegar_a_tu_dirección_particular_o_del_trabajo')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Cómo establecer tu dirección particular o de trabajo</Title>
				</Body>
			</Header>
			<Content>
				<Text>
					1    En tu computadora o celular, abre Google Maps y asegúrate de haber accedido a tu cuenta.{"\n"}{"\n"}
					2    Haz clic en el menú Menú y luego Tus lugares y luego Etiquetados.{"\n"}{"\n"}
					3    Elige Dirección particular o Dirección de trabajo.{"\n"}{"\n"}
					4    Escribe tu dirección particular o del trabajo y, luego, haz clic en Guardar.{"\n"}{"\n"}

					Nota: Si utilizas Maps en el modo básico, no podrás establecer tu dirección particular ni la de trabajo.{"\n"}{"\n"}
				</Text>
				<Image source={require('../Assets/Como_ingresar_una_direccion_de_partida.jpg')} style={{height: 210, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentXX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
