import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
  navigatetocontent1=()=>{
    this.props.navigation.navigate('GoogleC1')
  } 
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>Que es Google</Title>
          </Body>
        </Header>
        <Content>
          <Text>
          Google es una compañía estadounidense fundada en septiembre de 1998 cuyo producto principal es un motor de búsqueda creado por Larry Page y Sergey Brin.{"\n"}{"\n"}
           El término suele utilizarse como sinónimo de este buscador, el más usado en el mundo.{"\n"}{"\n"}
          </Text>
          <Text>
              Fuente: https://definicion.de/google/
          </Text>
          <Image source={require('../Images/googleapps.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
        </Content>
        <Footer>
          <FooterTab>
            <Button full warning onPress={this.navigatetocontent1}>
              <Text>Siguiente</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}