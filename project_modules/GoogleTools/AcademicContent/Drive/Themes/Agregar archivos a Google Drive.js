import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentNumeroX=()=>{
	this.props.navigation.navigate('GoogleD3')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Agregar archivos a Google Drive</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Si usas un ordenador, puedes subir archivos desde drive.google.com o desde el ordenador a carpetas privadas o compartidas.
			{"\n"}{"\n"}
			Ve a drive.google.com en tu ordenador.{"\n"}{"\n"}
			Arriba a la izquierda, haz clic en Nuevo. Subir archivo.{"\n"}{"\n"}
			Elige el archivo que quieras subir.Puedes subir, ver, compartir y editar archivos en Google Drive. Al subir un archivo a Google Drive, ocupará espacio en tu unidad de Drive, aunque lo subas a una carpeta que pertenezca a otra persona.{"\n"}{"\n"}
			Tipos de archivos{"\n"}{"\n"}

			Documentos{"\n"}
			Imágenes{"\n"}
			Audio{"\n"}
			Vídeo

			</Text>
			<Image source={require('../Assets/drive.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentNumeroX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
