import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentNumeroX=()=>{
		this.props.navigation.navigate('GoogleD4')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Compartir archivos en Drive</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Compartir un único archivo{"\n"}{"\n"}
				En un ordenador, ve a Drive, Documentos, Hojas de cálculo o Presentaciones de Google.{"\n"}{"\n"}
				Haz clic en el archivo que quieras compartir.{"\n"}{"\n"}
				Haz clic en Compartir o en el icono Compartir.{"\n"}{"\n"}
				Puede organizar, añadir y editar: si los usuarios han iniciado sesión en una cuenta de Google, pueden abrir, editar, eliminar o mover los archivos de la carpeta. También pueden añadirle archivos.{"\n"}{"\n"}
				Solo puede ver: los usuarios pueden ver la carpeta y abrir todos sus archivos.{"\n"}{"\n"}
			</Text>
			<Image source={require('../Assets/drive.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentNumeroX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
											