import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class AnatomyExample extends Component {
	navigatetocontentNumeroX=()=>{
		this.props.navigation.navigate('GoogleD2')
	} 
	render() {
	return (
		<Container>
		<Header>
			<Body>
			<Title>Que es Google Drive</Title>
			</Body>
		</Header>
		<Content>
			<Text>
			Google Drive es el lugar donde se accede a todos tus archivos, incluidos los documentos de Google Docs y los archivos locales que Utiliza Google Drive para guardar todo tipo de archivos, incluidos documentos, presentaciones, música, fotos y vídeos. {"\n"}{"\n"}
			Puedes abrir muchos tipos de archivo directamente en tu navegador, incluidos los archivos PDF, archivos Microsoft Office, vídeos de alta definición y muchos tipos de archivos de imagen, aunque no tengas instalado el programa correspondiente en tu ordenador.
			</Text>
			<Image source={require('../Assets/drive.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
		</Content>
		<Footer>
			<FooterTab>
			<Button full info onPress={this.navigatetocontentNumeroX}>
				<Text>Siguiente</Text>
			</Button>
			</FooterTab>
		</Footer>
		</Container>
		);
	}
}
											