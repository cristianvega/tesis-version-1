import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetocontentNumeroX=()=>{
			this.props.navigation.navigate('GoogleD3')
		} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Como ingresar a Google Drive</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Para ingresar y usar google drive
				{"\n"}{"\n"}
				Ir a google.com.ar y hacer click en aplicaciones y seleccionar Drive.{"\n"}{"\n"}
				Colocar contraseña y presiona Iniciar sesión.{"\n"}{"\n"}
				Puedes crear elementos.{"\n"}{"\n"}
				Puedes subir archivos o carpetas desde el disco rígido o pendrive.{"\n"}{"\n"}
				Clic sobre el botón derecho del mouse sobre archivo y lo puedes compartir.{"\n"}{"\n"}
				Listo!.
				</Text>
				<Image source={require('../Assets/drive.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button full info onPress={this.navigatetocontentNumeroX}>
					<Text>Siguiente</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
