import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
	export default class AnatomyExample extends Component {
		navigatetoEnd=()=>{
			this.props.navigation.navigate('ThemesGoogle')
			} 
		render() {
		return (
			<Container>
			<Header>
				<Body>
				<Title>Verificar archivos compartidos conmigo</Title>
				</Body>
			</Header>
			<Content>
				<Text>
				Añadir archivos a Mi unidad
				{"\n"}{"\n"}
				Ve a drive.google.com.{"\n"}{"\n"}
				En la parte izquierda, haz clic en Compartido conmigo.{"\n"}{"\n"}
				Haz clic en los archivos o las carpetas que quieras añadir a tu unidad.{"\n"}{"\n"}
				Arriba a la derecha, haz clic en Añadir a Mi unidad.{"\n"}{"\n"}
				Haz clic en Organizar.{"\n"}{"\n"}
				Elige la carpeta a la que quieras añadir los archivos.{"\n"}{"\n"}
				</Text>
				<Image source={require('../Assets/drive.jpeg')} style={{height: 200, width: 360 , flex: 1}}/>
			</Content>
			<Footer>
				<FooterTab>
				<Button success onPress={this.navigatetoEnd}>
					<Text>Finalizar</Text>
				</Button>
				</FooterTab>
			</Footer>
			</Container>
			);
		}
	}
