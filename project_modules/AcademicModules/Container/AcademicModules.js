import React, { Component } from 'react';
import { Image } from 'react-native';
import { 
    Container, 
    Header, 
    Content, 
    Card, 
    CardItem, 
    Thumbnail, 
    Text, 
    Button, 
    Icon, 
    Left, 
    Body, 
    Right, 
    Title,
    Footer,
    FooterTab 
} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
  Alert
} from "react-native";
import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase("tesis4.db");

export default class CardImageExample extends Component {

  constructor(){
    super()
    this.state = { 
        nombre: '', 
        apellido: '', 
        id: ''
    };
  }
  async UNSAFE_componentWillMount() {
    this.setState({ 
      nombre: this.props.navigation.state.params.StudentName,
      apellido: this.props.navigation.state.params.StudentSurname,
      id: this.props.navigation.state.params.IDStudent
    })
    var query = 'select count(*) as cuenta from EvaluationNotes where IDStudent='+this.props.navigation.state.params.IDStudent;
    var params = [];
    var queryresults =[];
    db.transaction((tx) => {
      tx.executeSql(query, params, (tx, results) => {
          console.log(results);
          queryresults=results.rows._array;
          // Alert.alert("Excelente", "Se realizado la consulta de conteo y la respuesta es: "+ queryresults[0].cuenta);
          if(queryresults[0].cuenta==0){
            db.transaction((tx) => {
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',1);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',2);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',3);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',4);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',5);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',6);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',7);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',8);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',9);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',10);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',11);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',12);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',13);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',14);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',15);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',16);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',17);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',18);');
              tx.executeSql('INSERT INTO EvaluationNotes (IDStudent,IDTheme) VALUES ('+this.state.id+',19);');
              Alert.alert("Info", "He insertado los datos de las notas para este usuario");
          });
        }
      }, function (tx, err) {
      });
    });
  }
  navigatetocontentthemes=()=>{
    
    this.props.navigation.navigate('Temary',{IDStudent: this.state.id,StudentName: this.state.nombre,StudentSurname: this.state.apellido})//Esta funcion me redirige segun lo que este en Routes.js
  } 
  navigatetoevaluationthemes=()=>{
      console.log(this.state.id);
      this.props.navigation.navigate('EvaluationTemary',{IDStudent: this.state.id,StudentName: this.state.nombre,StudentSurname: this.state.apellido})//Tiene que estar dentro del componente para que el componente la encuentre
  } 
  navigatetologin=()=>{
    this.props.navigation.navigate('Login')//Tiene que estar dentro del componente para que el componente la encuentre
  } 
  render() {
    return (
      <Container>
        <Header>
        <Title>Bienvenido, {this.state.nombre+' '+this.state.apellido}</Title>
        </Header>
        <Content>
        <Card>
            <CardItem>
              <Left>
                <Thumbnail source={require('../Images/MobileThumbnail.png')} />
                <Body>
                  <Text>Informática para Mobiles</Text>
                  <Text note>Ingresa para iniciar o continuar con el curso</Text>
                </Body>
              </Left>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetocontentthemes}>
            <CardItem cardBody>
              <Image source={require('../Images/MobileLayout.jpg')} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            </TouchableOpacity>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="clock" />
                  <Text>12 Horas</Text>
                </Button>
              </Left>
              <Right>
              <Button transparent>
                  <Icon active name="stats" />
                  <Text>50%</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={require('../Images/EvaluationThumbnail.png')} />
                <Body>
                  <Text>Evalúa tus conocimientos</Text>
                  <Text note>Ingresa para evaluar tus conocimientos</Text>
                </Body>
              </Left>
            </CardItem>
            <TouchableOpacity onPress={this.navigatetoevaluationthemes}>
            <CardItem cardBody>
              <Image source={require('../Images/EvaluationLayout.png')} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            </TouchableOpacity>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="clock" />
                  <Text>12 Horas</Text>
                </Button>
              </Left>
              <Right>
              <Button transparent>
                  <Icon active name="stats" />
                  <Text>50%</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Content>
        <Footer>
          <FooterTab>
            <Button full onPress={this.navigatetologin}>
              <Text>Salir</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}