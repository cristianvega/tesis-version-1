import React, { Component } from 'react';
import { 
  Container, 
  Card, 
  CardItem, 
  Content, 
  Button, 
  Body, 
  Icon, 
  Text, 
  Item, 
  Label, 
  Input, 
  Footer,
  FooterTab
} from 'native-base';
import styles from '../../Styles/styles';
import * as SQLite from 'expo-sqlite';
import {
  Alert
} from "react-native";

const db = SQLite.openDatabase("tesis4.db");

export default class Login extends Component {

  constructor(){
    super()
    this.state = { 
        loading: true, 
        user: '', 
        pass: '',
        dataQuery: [],
        IDStudent: '',
        StudentName: '',
        StudentSurname: ''
    };
  }
  async UNSAFE_componentWillMount() {
    var query = 'select count(*) as cuenta from Students';
    var params = [];
    var queryresults =[];
    db.transaction((tx) => {
      tx.executeSql(query, params, (tx, results) => {
          console.log(results);
          queryresults=results.rows._array;
          //Alert.alert("Excelente", "Se realizado la consulta de conteo y la respuesta es: "+ queryresults[0].cuenta);
      }, function (tx, err) {
            db.transaction(tx => {
              tx.executeSql('CREATE TABLE IF NOT EXISTS Country ( IDCountry   INTEGER      NOT NULL, CountryName VARCHAR (45) NOT NULL, active      BINARY       DEFAULT 1, PRIMARY KEY ( IDCountry AUTOINCREMENT ) );');
        
              tx.executeSql('INSERT INTO Country values (1,"Colombia",1);');
              tx.executeSql('INSERT INTO Country values (2,"Ecuador",1);');
              tx.executeSql('INSERT INTO Country values (3,"Bolivia",1);');
              tx.executeSql('INSERT INTO Country values (4,"Brasil",1);');
              tx.executeSql('INSERT INTO Country values (5,"Venezuela",1);');
              tx.executeSql('INSERT INTO Country values (6,"Peru",1);');
              
              tx.executeSql('CREATE TABLE IF NOT EXISTS Department ( IDDepartment   INTEGER      NOT NULL, DepartmentName VARCHAR (45) NOT NULL, IDCountry      INT          NOT NULL, active         BINARY       DEFAULT 1, PRIMARY KEY ( IDDepartment AUTOINCREMENT ), FOREIGN KEY ( IDCountry ) REFERENCES Country (IDCountry) );');
              
              tx.executeSql('INSERT INTO Department VALUES(4," Atlántico ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(5," Bogotá ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(6," Bolívar ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(7," Boyacá ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(8," Caldas ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(9," Caquetá ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(10," Casanare ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(11," Cauca ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(12," Cesar ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(13," Chocó ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(14," Córdoba ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(15," Cundinamarca ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(16," Guainía ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(17," Guaviare ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(18," Huila ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(19," La Guajira ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(20," Magdalena ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(21," Meta ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(22," Nariño ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(23," Norte de Santander ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(24," Putumayo ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(25," Quindío ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(26," Risaralda ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(27," San Andrés y Providencia ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(28," Santander ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(29," Sucre ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(30," Tolima ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(31," Valle del Cauca ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(32," Vaupés ",1,1);');
              tx.executeSql('INSERT INTO Department VALUES(33," Vichada ",1,1);');
              
              tx.executeSql('CREATE TABLE IF NOT EXISTS City ( IDCity   INTEGER      NOT NULL, CityName VARCHAR (45) NOT NULL, IDDepartment     INT          NOT NULL, active           BINARY       DEFAULT 1, PRIMARY KEY ( IDCity AUTOINCREMENT ), FOREIGN KEY ( IDDepartment ) REFERENCES Department (IDDepartment) );');
              
              tx.executeSql('INSERT INTO  City Values(1,"Leticia",1,1);');
              tx.executeSql('INSERT INTO  City Values(2,"Medellín",2,1);');
              tx.executeSql('INSERT INTO  City Values(3,"Arauca",3,1);');
              tx.executeSql('INSERT INTO  City Values(4,"Barranquilla",4,1);');
              tx.executeSql('INSERT INTO  City Values(5,"Bogotá",5,1);');
              tx.executeSql('INSERT INTO  City Values(6,"Cartagena de Indias",6,1);');
              tx.executeSql('INSERT INTO  City Values(7,"Tunja",7,1);');
              tx.executeSql('INSERT INTO  City Values(8,"Manizales",8,1);');
              tx.executeSql('INSERT INTO  City Values(9,"Florencia",9,1);');
              tx.executeSql('INSERT INTO  City Values(10,"Yopal",10,1);');
              tx.executeSql('INSERT INTO  City Values(11,"Popayán",11,1);');
              tx.executeSql('INSERT INTO  City Values(12,"Valledupar",12,1);');
              tx.executeSql('INSERT INTO  City Values(13,"Quibdó",13,1);');
              tx.executeSql('INSERT INTO  City Values(14,"Montería",14,1);');
              tx.executeSql('INSERT INTO  City Values(15,"Bogotá",15,1);');
              tx.executeSql('INSERT INTO  City Values(16,"Inírida",16,1);');
              tx.executeSql('INSERT INTO  City Values(17,"San José del Guaviare",17,1);');
              tx.executeSql('INSERT INTO  City Values(18,"Neiva",18,1);');
              tx.executeSql('INSERT INTO  City Values(19,"Riohacha",19,1);');
              tx.executeSql('INSERT INTO  City Values(20,"Santa Marta",20,1);');
              tx.executeSql('INSERT INTO  City Values(21,"Villavicencio",21,1);');
              tx.executeSql('INSERT INTO  City Values(22,"San Juan de Pasto",22,1);');
              tx.executeSql('INSERT INTO  City Values(23,"San José de Cúcuta",23,1);');
              tx.executeSql('INSERT INTO  City Values(24,"Mocoa",24,1);');
              tx.executeSql('INSERT INTO  City Values(25,"Armenia",25,1);');
              tx.executeSql('INSERT INTO  City Values(26,"Pereira",26,1);');
              tx.executeSql('INSERT INTO  City Values(27,"San Andrés",27,1);');
              tx.executeSql('INSERT INTO  City Values(28,"Bucaramanga",28,1);');
              tx.executeSql('INSERT INTO  City Values(29,"Sincelejo",29,1);');
              tx.executeSql('INSERT INTO  City Values(30,"Ibagué",30,1);');
              tx.executeSql('INSERT INTO  City Values(31,"Cali",31,1);');
              tx.executeSql('INSERT INTO  City Values(32,"Mitú",32,1);');
              tx.executeSql('INSERT INTO  City Values(33,"Puerto Carreño",33,1);');
              
              tx.executeSql('CREATE TABLE IF NOT EXISTS Genre ( IDGenre   INTEGER      NOT NULL, GenreName VARCHAR (45) NOT NULL, active    BINARY       DEFAULT 1, PRIMARY KEY ( IDGenre AUTOINCREMENT ) );');
              
              tx.executeSql('INSERT INTO Genre Values(1,"Masculino",1);');
              tx.executeSql('INSERT INTO Genre Values(2,"Femenino",1);');
              tx.executeSql('INSERT INTO Genre Values(3,"Trasgenero",1);');
              
              tx.executeSql('CREATE TABLE IF NOT EXISTS Subjects ( IDSubject          INTEGER       NOT NULL, SubjectName        VARCHAR (45)  NOT NULL, SubjectDescription VARCHAR (150) NOT NULL, active             BINARY        DEFAULT 1, PRIMARY KEY ( IDSubject AUTOINCREMENT ) );');
              
              tx.executeSql('INSERT INTO Subjects Values(1,"Redes Sociales","En este tema se enseñaran las aplicaciones para conectarse con mas personas",1);');
              tx.executeSql('INSERT INTO Subjects Values(2,"Movilidad","En este tema se enseñaran las aplicaciones para conseguir un transporte de un lugar a otro",1);');
              tx.executeSql('INSERT INTO Subjects Values(3,"Herramientas de Google","En este tema se enseñaran las aplicaciones que Google dispone para uso domestico",1);');
              tx.executeSql('INSERT INTO Subjects Values(4,"Manejo basico de celulares","En este tema se enseñaran las funcionalidades basicas de un celular",1);');
              
              tx.executeSql('CREATE TABLE IF NOT EXISTS Themes ( IDTheme          INTEGER       NOT NULL, ThemeName        VARCHAR (45)  NOT NULL, ThemeDescription VARCHAR (150) NOT NULL, IDSubject        INT           NOT NULL, active           BINARY        DEFAULT 1, PRIMARY KEY ( IDTheme AUTOINCREMENT ), FOREIGN KEY ( IDSubject ) REFERENCES Subjects (IDSubject) );');
              
              tx.executeSql('INSERT INTO Themes Values(1,"Facebook","Aplicacion para conectarse con amigos",1,1);');
              tx.executeSql('INSERT INTO Themes Values(2,"Whatsapp","Aplicacion para conectarse con amigos desde el celular",1,1);');
              tx.executeSql('INSERT INTO Themes Values(3,"Instagram","Aplicacion para ver fotos de amigos",1,1);');
              tx.executeSql('INSERT INTO Themes Values(4,"Twitter","Aplicacion para leer pequeños textos de amigos",1,1);');
              tx.executeSql('INSERT INTO Themes Values(5,"Uber","Aplicacion para transportarse en carro",2,1);');
              tx.executeSql('INSERT INTO Themes Values(6,"Picap","Aplicacion para transportarse en moto",2,1);');
              tx.executeSql('INSERT INTO Themes Values(7,"Beat","Aplicacion para transportarse en carro",2,1);');
              tx.executeSql('INSERT INTO Themes Values(8,"Cuper","Aplicacion para transportarse en moto",2,1);');
              tx.executeSql('INSERT INTO Themes Values(9,"Gmail","Aplicacion para transportarse en carro",2,1);');
              tx.executeSql('INSERT INTO Themes Values(10,"Google Maps","Aplicacion para recibir correos",3,1);');
              tx.executeSql('INSERT INTO Themes Values(11,"Google Drive","Aplicacion para guardar archivos en internet",3,1);');
              tx.executeSql('INSERT INTO Themes Values(12,"Buscador de Google","Aplicacion para buscar temas en internet",3,1);');
              tx.executeSql('INSERT INTO Themes Values(13,"PlayStore de Google","Aplicacion para instalar software",3,1);');
              tx.executeSql('INSERT INTO Themes Values(14,"Play Music de Google","Aplicacion para escuchar musica",3,1);');
              tx.executeSql('INSERT INTO Themes Values(15,"Activar internet","Conectarse a internet wifi o datos",4,1);');
              tx.executeSql('INSERT INTO Themes Values(16,"Hacer llamadas","Realizar llamadas",4,1);');
              tx.executeSql('INSERT INTO Themes Values(17,"Desinstalar aplicaciones","Desinstalar aplicaciones",4,1);');
              tx.executeSql('INSERT INTO Themes Values(18,"Manejar alarmas","Crear alarmas y usarlas",4,1);');
              tx.executeSql('INSERT INTO Themes Values(19,"Utilizar iconos","Utilizar los iconos predeterminados en el celular",4,1);');
              
              tx.executeSql('CREATE TABLE IF NOT EXISTS STUDENTS ( \
                IDStudent       INTEGER      NOT NULL\
                , StudentName     VARCHAR (45) NOT NULL\
                , StudentName2    VARCHAR (45)\
                , StudentSurname  VARCHAR (45) NOT NULL\
                , StudentSurname2 VARCHAR (45)\
                , IDCountry INT\
                , IDCity INT\
                , StudentPhone    VARCHAR (13) NOT NULL\
                , StudentAge         INT          \
                , IDGenre         INT          NOT NULL\
                , StudentEmail    VARCHAR (60)\
                , StudentUser    VARCHAR (60)\
                , StudentPassword    VARCHAR (100)\
                , active          BINARY       DEFAULT 1\
                , PRIMARY KEY ( IDStudent AUTOINCREMENT )\
                , FOREIGN KEY ( IDCity ) REFERENCES City (IDCity)\
                , FOREIGN KEY ( IDGenre ) REFERENCES Genre (IDGenre) );');

              tx.executeSql('CREATE TABLE IF NOT EXISTS EvaluationNotes ( \
                IDNote    INTEGER    NOT NULL\
                , IDStudent INTEGER    NOT NULL\
                , IDTheme   INTEGER    NOT NULL\
                , Note      DOUBLE     DEFAULT 0\
                , PRIMARY KEY ( IDNote AUTOINCREMENT )\
                , FOREIGN KEY ( IDStudent ) REFERENCES STUDENTS (IDStudent)\
                , FOREIGN KEY ( IDTheme ) REFERENCES Themes (IDTheme));');
            
              tx.executeSql('CREATE TABLE IF NOT EXISTS Test ( IDRegistry INTEGER, active     BINARY DEFAULT 1, PRIMARY KEY ( IDRegistry AUTOINCREMENT ) ) ;');
            });
            // Alert.alert("Carga inicial", "Hice la carga inicial de datos porque no hay conteo ");
            return;
      });
    });
  }

  navigatetoregistry=()=>{
      this.props.navigation.navigate('Register')//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  navigatetomodules=(id,name,surname)=>{
    this.props.navigation.navigate('Modules',{IDStudent: id,StudentName: name,StudentSurname:surname})//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 
  navigatetotest=()=>{
    this.props.navigation.navigate('TestQuery')//Esta funcion es importantisima porque es la que me dice a donde voy a navegar... Register esta contenida en la funcion de App.js de createStackNavigator
  } 

  verifyUser(user,pass){

    if(user!="" && pass!=""){
      var query = 'select StudentName as name, StudentSurname as surname, IDStudent as ID from Students where StudentUser =? and StudentPassword=?';
      var params = [user,pass];
      var nuevoarray = [];
      db.transaction(
        (tx) => {
        tx.executeSql(
            query, params, 
            (tx, results) => {
            if (results.rows._array.length > 0) {
                nuevoarray= results.rows._array;
                console.log(nuevoarray);
                this.setState({
                    dataQuery: results.rows._array,
                });
                this.navigatetomodules(nuevoarray[0].ID,nuevoarray[0].name,nuevoarray[0].surname);
                console.log(nuevoarray[0].ID);
            }       
            else{
              Alert.alert("Por favor verifica tu usuario y contraseña");
            }
        }, function (tx, err) {
            Alert.alert("Existe un error: "+err);
        });
      });
    }
    else{
      Alert.alert("Hay un espacio vacio");
    }
  }
    
  render() {
    return (
      <Container>
        <Content padder contentContainerStyle={styles.content}>
          <Card>
            <CardItem header bordered>
              <Text style={styles.textcenter}>Aplicacion de aprendizaje MovilMigo</Text>
            </CardItem>
            <CardItem bordered>
              <Body style={styles.body}>
                <Item floatingLabel>
                  <Icon active name='person' />
                  <Label>Usuario</Label>
                  <Input 
                    onChangeText={(val) => this.setState({ user: val })} value={this.state.user}
                  />
                </Item>
                <Item floatingLabel>
                  <Icon active name='key' />
                  <Label>Contraseña</Label>
                  <Input
                    onChangeText={(val) => this.setState({ pass: val })} 
                    value={this.state.pass} 
                    secureTextEntry={true}
                  />
              </Item>
              </Body>
            </CardItem>
            <CardItem footer bordered>
            <Button iconLeft 
            onPress={() => {
              this.verifyUser(this.state.user,this.state.pass);
              }}
            >
                <Icon name='navigate' />
                <Text>Ingresar</Text>
                </Button>
                <Button success style={styles.button} onPress={this.navigatetoregistry}>
                <Icon name='navigate' />
                <Text>Registrate</Text>
                </Button>
            </CardItem>
          </Card>
        </Content>
        <Footer style={styles.updatesfooter}>
                <Text style={styles.updates}>Ultimas actualizaciones V.1.12: Visualizacion de notas completado</Text>
        </Footer>
      </Container>
    );
  }
}