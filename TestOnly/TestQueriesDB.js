import React, { Component } from 'react';
import {ScrollView, View, Alert} from 'react-native';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Text, Input, Spinner, ListItem, Separator  } from 'native-base';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase("tesis4.db");
export default class AnatomyExample extends Component {
    constructor(){
        super()
        this.state = { 
            loading: true, 
            dataQuery: [], 
            Query: '',
        };
    }
    async UNSAFE_componentWillMount() {
        this.setState({ loading: false });
    }
    async UNSAFE_componentDidMount() {
        const { Query } = this.state;
        await this.realizarQuery(Query);
    }
    handleSearch() {
        const { Query } = this.state;
        if (Query != "") {
            this.realizarQuery(Query);
        }
        else {
            Alert.alert("Cuidado", "Este query esta vacio");
        }
    }
    autoquery() {
        this.setState({
            Query: 'select ID- as id, -name as Description from ---'
        });
    }

    realizarQuery(Query) {
        var query = Query;
        var params = [];
        var nuevoarray = [];
        db.transaction(
            (tx) => {
            tx.executeSql(
                query, params, 
                (tx, results) => {
                if (results.rows._array.length > 0) {
                    nuevoarray= results.rows._array;
                    console.log(nuevoarray);
                    this.setState({
                        dataQuery: results.rows._array
                    });
                }
                               
                Alert.alert("He realizado la consulta");
            }, function (tx, err) {
                Alert.alert("Existe un error: "+err);
            });
        });
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner color="green" />
                </View>
            );
        }
        const listItem = this.state.dataQuery.map((item) =>
        <View key={item.id}>
            <Collapse style={{ marginBottom: 10, marginTop: 10 }}>
                <CollapseHeader>
                    <Separator bordered>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontSize: 14, color: '#000000' }}>{item.Description}</Text>
                            <Text>Registro de nombre: </Text>
                        </View>
                    </Separator>
                </CollapseHeader>
                <CollapseBody>
                    <ListItem style={{ marginBottom: 3, marginTop: 3 }}>
                        <Text><Text> ID : </Text>{item.id}</Text>
                    </ListItem>
                    <ListItem style={{ marginBottom: 3, marginTop: 3 }}>
                        <Text><Text> Description : </Text>{item.Description}</Text>
                    </ListItem>
                </CollapseBody>
            </Collapse>
        </View>
    );
        return (
        <Container>
            <Header>
            <Body>
                <Title>Pantalla para realizar queries</Title>
            </Body>
            </Header>
            <Content>
            <Input
                    onChangeText={(val) => this.setState({ Query: val })} value={this.state.Query}
                    placeholder='Ingresa el query con alias id y description'
                    leftIconContainerStyle={{ marginRight: 15 }}
                    inputContainerStyle={{ marginTop: 45, width: 330, marginLeft: 30 }}
            />
            <Button
                onPress={() => {
                this.autoquery();
                }}
            >
                <Text>Autocompletar Query</Text>
            </Button>
                <ScrollView>
                    <View>
                        {listItem}
                    </View>
                </ScrollView>
            </Content>
            <Footer>
            <FooterTab>
                    <Button
                        onPress={() => {
                        this.handleSearch();
                        }}
                    >
                <Text>Realizar Query</Text>
                </Button>
            </FooterTab>
            </Footer>
        </Container>
        );
    }
}