import React from "react";
import Setup from "./src/boot/setup.js";
import {
  Alert
} from "react-native";
import * as Updates from 'expo-updates'
import * as Network from 'expo-network';


export default class App extends React.Component {
  async componentDidMount(){
    try {
      const update = await Updates.checkForUpdateAsync();
      const wifi = await Network.getNetworkStateAsync();
      if (update.isAvailable && wifi.type == 'WIFI') {
        await Updates.fetchUpdateAsync()
        // NOTIFY USER HERE
        Alert.alert("Nueva version","Existe una nueva version de esta aplicacion, no te preocupes, la instalaremos automaticamente");
        Updates.reloadAsync();
      }
      // else{
      //   Alert.alert("No hay nuevas versiones","Tipo de conexion: "+wifi.type+" Update: "+update.isAvailable);
      // }
    } catch (e) {
      Alert.alert("Error actualizacion","Se ha producido un error al descargar la actualizacion, por favor cierra y abre nuevamente esta aplicacion: "+e);
        // HANDLE ERROR HERE
    }
  }
  render() {
    return <Setup />;
  }
}