import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
//Declaracion de vistas para usar en navigation
import Login from '../project_modules/Login/Container/Login';
import Register from '../project_modules/Register/Container/Register';
import Modules from '../project_modules/AcademicModules/Container/AcademicModules';
import Temary from '../project_modules/Temary/Container/Temary';
import EvaluationTemary from '../project_modules/EvaluationTemary/Container/EvaluationTemary';
import ThemesSocialMedia from '../project_modules/Temary/Container/ThemesSocialMedia';
import ThemesMovility from '../project_modules/Temary/Container/ThemesMovility';
import ThemesGoogle from '../project_modules/Temary/Container/ThemesGoogle';
import ThemesBasic from '../project_modules/Temary/Container/ThemesBasic';
//Navegaciones a los contenidos de los temas
// Manejo Basico de Celulares
import BasicIntro from '../project_modules/BasicHandle/AcademicContent/Introduction';
// Google
import GoogleIntro from '../project_modules/GoogleTools/AcademicContent/Introduction';
//Gmail
import GoogleC1 from '../project_modules/GoogleTools/AcademicContent/Gmail/Themes/Que es Gmail'
import GoogleC2 from '../project_modules/GoogleTools/AcademicContent/Gmail/Themes/Registro e inicio de sesión Gmail';
import GoogleC3 from '../project_modules/GoogleTools/AcademicContent/Gmail/Themes/Envio de mensajes en Gmail';
import GoogleC4 from '../project_modules/GoogleTools/AcademicContent/Gmail/Themes/Carpetas de correos';
import GoogleC5 from '../project_modules/GoogleTools/AcademicContent/Gmail/Themes/Personalización en gmail';
//Drive
import GoogleD1 from '../project_modules/GoogleTools/AcademicContent/Drive/Themes/Que es Google Drive';
import GoogleD2 from '../project_modules/GoogleTools/AcademicContent/Drive/Themes/Como ingresar a Google Drive';
import GoogleD3 from '../project_modules/GoogleTools/AcademicContent/Drive/Themes/Agregar archivos a Google Drive';
import GoogleD4 from '../project_modules/GoogleTools/AcademicContent/Drive/Themes/Compartir archivos en Drive';
import GoogleD5 from '../project_modules/GoogleTools/AcademicContent/Drive/Themes/Verificar archivos compartidos conmigo';
//Maps
import GoogleTools_Google_Maps_Que_es_Google_Maps from '../project_modules/GoogleTools/AcademicContent/Google_Maps/Themes/Que_es_Google_Maps';
import GoogleTools_Google_Maps_Cómo_establecer_tu_dirección_particular_o_de_trabajo from '../project_modules/GoogleTools/AcademicContent/Google_Maps/Themes/Cómo_establecer_tu_dirección_particular_o_de_trabajo';
import GoogleTools_Google_Maps_Cómo_obtener_instrucciones_para_llegar_a_tu_dirección_particular_o_del_trabajo from '../project_modules/GoogleTools/AcademicContent/Google_Maps/Themes/Cómo_obtener_instrucciones_para_llegar_a_tu_dirección_particular_o_del_trabajo';
import GoogleTools_Google_Maps_Cómo_cambiar_tu_dirección_particular_o_la_del_trabajo from '../project_modules/GoogleTools/AcademicContent/Google_Maps/Themes/Cómo_cambiar_tu_dirección_particular_o_la_del_trabajo';
import GoogleTools_Google_Maps_Cómo_solucionar_problemas_para_ver_tu_dirección_particular_y_del_trabajo_en_Maps from '../project_modules/GoogleTools/AcademicContent/Google_Maps/Themes/Cómo_solucionar_problemas_para_ver_tu_dirección_particular_y_del_trabajo_en_Maps';
//Music
import GoogleTools_Google_Music_Que_es_Google_Music from '../project_modules/GoogleTools/AcademicContent/Google_Music/Themes/Que_es_Google_Music ';
import GoogleTools_Google_Music_Crear_listas_de_reproducción_y_estaciones_de_radio from '../project_modules/GoogleTools/AcademicContent/Google_Music/Themes/Crear_listas_de_reproducción_y_estaciones_de_radio';
import GoogleTools_Google_Music_Cómo_configurar_la_app_de_Google_Play_Música from '../project_modules/GoogleTools/AcademicContent/Google_Music/Themes/Cómo_configurar_la_app_de_Google_Play_Música';
import GoogleTools_Google_Music_Agregar_tu_colección_personal from '../project_modules/GoogleTools/AcademicContent/Google_Music/Themes/Agregar_tu_colección_personal';
import GoogleTools_Google_Music_Transmitir_música from '../project_modules/GoogleTools/AcademicContent/Google_Music/Themes/Transmitir_música';
// Movilidad
import MovIntro from '../project_modules/Movility/AcademicContent/Introduction';
// Uber
import Movility_Uber_Que_es_Uber from '../project_modules/Movility/AcademicContent/Uber/Themes/Que_es_Uber';
import Movility_Uber_Como_solicitar_un_servicio from '../project_modules/Movility/AcademicContent/Uber/Themes/Como_solicitar_un_servicio';
import Movility_Uber_Como_pagar_un_servicio from '../project_modules/Movility/AcademicContent/Uber/Themes/Como_pagar_un_servicio';
import Movility_Uber_Como_agregar_direcciones_en_Uber from '../project_modules/Movility/AcademicContent/Uber/Themes/Como_agregar_direcciones_en_Uber';
//Picap
import Movility_Picap_Que_es_Picap from '../project_modules/Movility/AcademicContent/Picap/Themes/Que_es_Picap';
import Movility_Picap_Como_solicitar_un_servicio_de_Picup from '../project_modules/Movility/AcademicContent/Picap/Themes/Como_solicitar_un_servicio_de_Picup';
import Movility_Picap_Como_registrarse_en_picup_ from '../project_modules/Movility/AcademicContent/Picap/Themes/Como_registrarse_en_picup_';
import Movility_Picap_Como_pagar_un_servicio_en_Picup_ from '../project_modules/Movility/AcademicContent/Picap/Themes/Como_pagar_un_servicio_en_Picup_';
import Movility_Picap_Como_agregar_direcciones_en_Picup from '../project_modules/Movility/AcademicContent/Picap/Themes/Como_agregar_direcciones_en_Picup';
//Beat
import Movility_Beat_Que_es_Beat from '../project_modules/Movility/AcademicContent/Beat/Themes/Que_es_Beat';
import Movility_Beat_Como_registrarse_en_Beat from '../project_modules/Movility/AcademicContent/Beat/Themes/Como_registrarse_en_Beat';
import Movility_Beat_Como_solicitar_un_servicio_de_Beat from '../project_modules/Movility/AcademicContent/Beat/Themes/Como_solicitar_un_servicio_de_Beat';
import Movility_Beat_Como_pagar_un_servicio_de_Beat from '../project_modules/Movility/AcademicContent/Beat/Themes/Como_pagar_un_servicio_de_Beat';
import Movility_Beat_Como_cambiar_el_destino_ from '../project_modules/Movility/AcademicContent/Beat/Themes/Como_cambiar_el_destino_';
//Cuper
import Movility_Cuper_Que_es_Cuper from '../project_modules/Movility/AcademicContent/Cuper/Themes/Que_es_Cuper';
import Movility_Cuper_Como_funciona_Cuper from '../project_modules/Movility/AcademicContent/Cuper/Themes/Como_funciona_Cuper';
import Movility_Cuper_Como_solicitar_un_servicio from '../project_modules/Movility/AcademicContent/Cuper/Themes/Como_solicitar_un_servicio';
import Movility_Cuper_Como_cambiar_mi_punto_de_Partida from '../project_modules/Movility/AcademicContent/Cuper/Themes/Como_cambiar_mi_punto_de_Partida';
import Movility_Cuper_Como_pagar_un_servicio_Cuper from '../project_modules/Movility/AcademicContent/Cuper/Themes/Como_pagar_un_servicio_Cuper';
// Redes sociales
import SocialIntro from '../project_modules/SocialMedia/AcademicContent/Introduction';
//Facebook
import SocialF0 from '../project_modules/SocialMedia/AcademicContent/Facebook/Themes/Que_es_Facebook';
import SocialF1 from '../project_modules/SocialMedia/AcademicContent/Facebook/Themes/Agregar Amigos';
import SocialF2 from '../project_modules/SocialMedia/AcademicContent/Facebook/Themes/Añadir amigos';
import SocialF3 from '../project_modules/SocialMedia/AcademicContent/Facebook/Themes/Mensajeria';
import SocialF4 from '../project_modules/SocialMedia/AcademicContent/Facebook/Themes/Publicación de contenidos';
import SocialF5 from '../project_modules/SocialMedia/AcademicContent/Facebook/Themes/Registro e inicio de sesión';
import SocialF6 from '../project_modules/SocialMedia/AcademicContent/Facebook/Themes/Sugerencias de Amistad';
//WhatsApp
import SocialW0 from '../project_modules/SocialMedia/AcademicContent/Whatsapp/Themes/Que es Whatsapp';
import SocialW1 from '../project_modules/SocialMedia/AcademicContent/Whatsapp/Themes/Registro e inicio de sesión Whatsapp';
import SocialW2 from '../project_modules/SocialMedia/AcademicContent/Whatsapp/Themes/Agregar contactos';
import SocialW3 from '../project_modules/SocialMedia/AcademicContent/Whatsapp/Themes/Enviar Mensajes';
import SocialW4 from '../project_modules/SocialMedia/AcademicContent/Whatsapp/Themes/Cambiar mi foto del perfil, nombre, Info. o estado en WhatsApp';
import SocialW5 from '../project_modules/SocialMedia/AcademicContent/Whatsapp/Themes/Personalizar';
//Twitter
import SocialT0 from '../project_modules/SocialMedia/AcademicContent/Twitter/Themes/Que es Twitter';
import SocialT1 from '../project_modules/SocialMedia/AcademicContent/Twitter/Themes/Crear un Twitter';
import SocialT2 from '../project_modules/SocialMedia/AcademicContent/Twitter/Themes/Enviar Mensajes en Twitter';
import SocialT3 from '../project_modules/SocialMedia/AcademicContent/Twitter/Themes/seguir y retwittear';
import SocialT4 from '../project_modules/SocialMedia/AcademicContent/Twitter/Themes/Personalizar Twitter';
//Instagram
import SocialI0 from '../project_modules/SocialMedia/AcademicContent/Instagram/Themes/Que es instagram';
import SocialI1 from '../project_modules/SocialMedia/AcademicContent/Instagram/Themes/Registro e inicio de sesión Instagram';
import SocialI2 from '../project_modules/SocialMedia/AcademicContent/Instagram/Themes/Enviar Mensajes en Instagram';
import SocialI3 from '../project_modules/SocialMedia/AcademicContent/Instagram/Themes/Publicar fotos en Instagram';
import SocialI4 from '../project_modules/SocialMedia/AcademicContent/Instagram/Themes/Seguir y aceptar contactos Instagramam';
//Navegaciones a las evaluaciones de los temas
import FacebookEvaluation from '../project_modules/Evaluations/Container/Facebook/EvaluationFacebook';
//Navegacion a visualizacion de notas
import ToNotes from '../project_modules/Evaluations/Container/GeneralNotes/Notes';
//Navegacion a test
import TestQuery from '../TestOnly/TestQueriesDB';























const Navigator = createStackNavigator({
    Login: {
      screen: Login,
      navigationOptions : {
        title: 'Bienvenido a MovilMigo'
      }
    },
    Register: {
      screen: Register,
      navigationOptions: {
        title: 'Registrate para continuar'
      }
    },
    Modules: {
      screen: Modules,
      navigationOptions: {
        title: 'Modulos Academicos'
      }
    },
    Temary: {
      screen: Temary,
      navigationOptions: {
        title: 'Temas a estudiar'
      }
    },
    EvaluationTemary: {
      screen: EvaluationTemary,
      navigationOptions: {
        title: 'Temas a evaluar'
      }
    },
    ThemesSocialMedia: {
      screen: ThemesSocialMedia,
      navigationOptions: {
        title: 'Redes Sociales'
      }
    },
    ThemesMovility: {
      screen: ThemesMovility,
      navigationOptions: {
        title: 'Movilidad'
      }
    },
    ThemesGoogle: {
      screen: ThemesGoogle,
      navigationOptions: {
        title: 'Herramientas de Google'
      }
    },
    ThemesBasic: {
      screen: ThemesBasic,
      navigationOptions: {
        title: 'Manejo basico celular'
      }
    },
    //Rutas de los contenidos
    // Basico
    BasicIntro: {
      screen: BasicIntro,
      navigationOptions: {
        title: 'Manejo basico celular'
      }
    },
    // Herramientas de Google
    GoogleIntro: {
      screen: GoogleIntro,
      navigationOptions: {
        title: 'Manejo basico celular'
      }
    },
    //Gmail
    GoogleC1:{
      screen: GoogleC1,
      navigationOptions: {
        title: 'Gmail'
      }
    },
    GoogleC2:{
      screen: GoogleC2,
      navigationOptions: {
        title: 'Gmail'
      }
    },
    GoogleC3:{
      screen: GoogleC3,
      navigationOptions: {
        title: 'Gmail'
      }
    },
    GoogleC4:{
      screen: GoogleC4,
      navigationOptions: {
        title: 'Gmail'
      }
    },
    GoogleC5:{
      screen: GoogleC5,
      navigationOptions: {
        title: 'Gmail'
      }
    },
    //Drive
    GoogleD1:{
      screen: GoogleD1,
      navigationOptions: {
        title: 'Drive'
      }
    },
    GoogleD2:{
      screen: GoogleD2,
      navigationOptions: {
        title: 'Drive'
      }
    },
    GoogleD3:{
      screen: GoogleD3,
      navigationOptions: {
        title: 'Drive'
      }
    },
    GoogleD4:{
      screen: GoogleD4,
      navigationOptions: {
        title: 'Drive'
      }
    },
    GoogleD5:{
      screen: GoogleD5,
      navigationOptions: {
        title: 'Drive'
      }
    },
    //Maps
    GoogleTools_Google_Maps_Que_es_Google_Maps: {
      screen: GoogleTools_Google_Maps_Que_es_Google_Maps,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Maps_Cómo_establecer_tu_dirección_particular_o_de_trabajo: {
      screen: GoogleTools_Google_Maps_Cómo_establecer_tu_dirección_particular_o_de_trabajo,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Maps_Cómo_obtener_instrucciones_para_llegar_a_tu_dirección_particular_o_del_trabajo: {
      screen: GoogleTools_Google_Maps_Cómo_obtener_instrucciones_para_llegar_a_tu_dirección_particular_o_del_trabajo,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Maps_Cómo_cambiar_tu_dirección_particular_o_la_del_trabajo: {
      screen: GoogleTools_Google_Maps_Cómo_cambiar_tu_dirección_particular_o_la_del_trabajo,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Maps_Cómo_solucionar_problemas_para_ver_tu_dirección_particular_y_del_trabajo_en_Maps: {
      screen: GoogleTools_Google_Maps_Cómo_solucionar_problemas_para_ver_tu_dirección_particular_y_del_trabajo_en_Maps,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    //Music
    GoogleTools_Google_Music_Que_es_Google_Music: {
      screen: GoogleTools_Google_Music_Que_es_Google_Music,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Music_Crear_listas_de_reproducción_y_estaciones_de_radio: {
      screen: GoogleTools_Google_Music_Crear_listas_de_reproducción_y_estaciones_de_radio,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Music_Cómo_configurar_la_app_de_Google_Play_Música: {
      screen: GoogleTools_Google_Music_Cómo_configurar_la_app_de_Google_Play_Música,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Music_Agregar_tu_colección_personal: {
      screen: GoogleTools_Google_Music_Agregar_tu_colección_personal,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    GoogleTools_Google_Music_Transmitir_música: {
      screen: GoogleTools_Google_Music_Transmitir_música,
      navigationOptions: {
        title: 'GoogleTools'
      }
    },
    //Movilidad
    MovIntro: {
      screen: MovIntro,
      navigationOptions: {
        title: 'Manejo basico celular'
      }
    },
    //Uber
    Movility_Uber_Que_es_Uber: {
      screen: Movility_Uber_Que_es_Uber,
      navigationOptions: {
        title: 'Uber'
      }
    },
    Movility_Uber_Como_solicitar_un_servicio: {
      screen: Movility_Uber_Como_solicitar_un_servicio,
      navigationOptions: {
        title: 'Uber'
      }
    },
    Movility_Uber_Como_pagar_un_servicio: {
      screen: Movility_Uber_Como_pagar_un_servicio,
      navigationOptions: {
        title: 'Uber'
      }
    },
    Movility_Uber_Como_agregar_direcciones_en_Uber: {
      screen: Movility_Uber_Como_agregar_direcciones_en_Uber,
      navigationOptions: {
        title: 'Uber'
      }
    },
    //Picap
    Movility_Picap_Que_es_Picap: {
      screen: Movility_Picap_Que_es_Picap,
      navigationOptions: {
        title: 'Picap'
      }
    },
    Movility_Picap_Como_solicitar_un_servicio_de_Picup: {
      screen: Movility_Picap_Como_solicitar_un_servicio_de_Picup,
      navigationOptions: {
        title: 'Picap'
      }
    },
    Movility_Picap_Como_registrarse_en_picup_: {
      screen: Movility_Picap_Como_registrarse_en_picup_,
      navigationOptions: {
        title: 'Picap'
      }
    },
    Movility_Picap_Como_pagar_un_servicio_en_Picup_: {
      screen: Movility_Picap_Como_pagar_un_servicio_en_Picup_,
      navigationOptions: {
        title: 'Picap'
      }
    },
    Movility_Picap_Como_agregar_direcciones_en_Picup: {
      screen: Movility_Picap_Como_agregar_direcciones_en_Picup,
      navigationOptions: {
        title: 'Picap'
      }
    },
    //Beat
    Movility_Beat_Que_es_Beat: {
      screen: Movility_Beat_Que_es_Beat,
      navigationOptions: {
        title: 'Beat'
      }
    },
    Movility_Beat_Como_registrarse_en_Beat: {
      screen: Movility_Beat_Como_registrarse_en_Beat,
      navigationOptions: {
        title: 'Movility'
      }
    },
    Movility_Beat_Como_solicitar_un_servicio_de_Beat: {
      screen: Movility_Beat_Como_solicitar_un_servicio_de_Beat,
      navigationOptions: {
        title: 'Movility'
      }
    },
    Movility_Beat_Como_pagar_un_servicio_de_Beat: {
      screen: Movility_Beat_Como_pagar_un_servicio_de_Beat,
      navigationOptions: {
        title: 'Movility'
      }
    },
    Movility_Beat_Como_cambiar_el_destino_: {
      screen: Movility_Beat_Como_cambiar_el_destino_,
      navigationOptions: {
        title: 'Movility'
      }
    },
    //Cuper
    Movility_Cuper_Que_es_Cuper: {
      screen: Movility_Cuper_Que_es_Cuper,
      navigationOptions: {
        title: 'Movility'
      }
    },
    Movility_Cuper_Como_funciona_Cuper: {
      screen: Movility_Cuper_Como_funciona_Cuper,
      navigationOptions: {
        title: 'Movility'
      }
    },
    Movility_Cuper_Como_solicitar_un_servicio: {
      screen: Movility_Cuper_Como_solicitar_un_servicio,
      navigationOptions: {
        title: 'Movility'
      }
    },
    Movility_Cuper_Como_cambiar_mi_punto_de_Partida: {
      screen: Movility_Cuper_Como_cambiar_mi_punto_de_Partida,
      navigationOptions: {
        title: 'Movility'
      }
    },
    Movility_Cuper_Como_pagar_un_servicio_Cuper: {
      screen: Movility_Cuper_Como_pagar_un_servicio_Cuper,
      navigationOptions: {
        title: 'Movility'
      }
    },
    //Redes sociales
    SocialIntro: {
      screen: SocialIntro,
      navigationOptions: {
        title: 'Manejo basico celular'
      }
    },
    //Facebook
    SocialF0: {
      screen: SocialF0,
      navigationOptions: {
        title: 'Facebook'
      }
    },
    SocialF1: {
      screen: SocialF1,
      navigationOptions: {
        title: 'Facebook'
      }
    },
    SocialF2: {
      screen: SocialF2,
      navigationOptions: {
        title: 'Facebook'
      }
    },
    SocialF3: {
      screen: SocialF3,
      navigationOptions: {
        title: 'Facebook'
      }
    },
    SocialF4: {
      screen: SocialF4,
      navigationOptions: {
        title: 'Facebook'
      }
    },
    SocialF5: {
      screen: SocialF5,
      navigationOptions: {
        title: 'Facebook'
      }
    },
    SocialF6: {
      screen: SocialF6,
      navigationOptions: {
        title: 'Facebook'
      }
    },
    //Evaluacion de facebook
    FacebookEvaluation: {
      screen: FacebookEvaluation,
      navigationOptions: {
        title: 'Manejo basico celular'
      }
    },
    //WhatsApp
    SocialWO: {
      screen: SocialW0,
      navigationOptions: {
        title: 'WhatsApp'
      }
    },
    SocialW1: {
      screen: SocialW1,
      navigationOptions: {
        title: 'WhatsApp'
      }
    },
    SocialW2: {
      screen: SocialW2,
      navigationOptions: {
        title: 'WhatsApp'
      }
    },
    SocialW3: {
      screen: SocialW3,
      navigationOptions: {
        title: 'WhatsApp'
      }
    },
    SocialW4: {
      screen: SocialW4,
      navigationOptions: {
        title: 'WhatsApp'
      }
    },
    SocialW5: {
      screen: SocialW5,
      navigationOptions: {
        title: 'WhatsApp'
      }
    },
    //Evaluacion de WhatsApp
    //Twitter
    SocialTO: {
      screen: SocialT0,
      navigationOptions: {
        title: 'Twitter'
      }
    },
    SocialT1: {
      screen: SocialT1,
      navigationOptions: {
        title: 'Twitter'
      }
    },
    SocialT2: {
      screen: SocialT2,
      navigationOptions: {
        title: 'Twitter'
      }
    },
    SocialT3: {
      screen: SocialT3,
      navigationOptions: {
        title: 'Twitter'
      }
    },
    SocialT4: {
      screen: SocialT4,
      navigationOptions: {
        title: 'Twitter'
      }
    },
    //Evaluacion de Twitter
    //Instagram
    SocialIO: {
      screen: SocialI0,
      navigationOptions: {
        title: 'Instagram'
      }
    },
    SocialI1: {
      screen: SocialI1,
      navigationOptions: {
        title: 'Instagram'
      }
    },
    SocialI2: {
      screen: SocialI2,
      navigationOptions: {
        title: 'Instagram'
      }
    },
    SocialI3: {
      screen: SocialI3,
      navigationOptions: {
        title: 'Instagram'
      }
    },
    SocialI4: {
      screen: SocialI4,
      navigationOptions: {
        title: 'Instagram'
      }
    },
    //Evaluacion de Instagram

    // Visualizacion de notas
    ToNotes: {
      screen: ToNotes,
      navigationOptions: {
        title: 'Tus Notas'
      }
    },
    //Test Query
    TestQuery: {
      screen: TestQuery,
      navigationOptions: {
        title: 'Test Query'
      }
    },
  },
  {
  //  mode: 'modal',
  //  headerMode: 'none' 
  }
);

const App = createAppContainer(Navigator);

export default App;